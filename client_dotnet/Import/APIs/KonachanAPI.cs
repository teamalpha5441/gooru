using System;
using System.Xml;
using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class KonachanAPI : API
    {
        private string _Domain;
        private APIConfig _Config;
        private Regex _URLRegex;
        private Regex _IDRegex;

        public override APIConfig Config { get { return _Config; } }

        public KonachanAPI(Downloader Downloader, bool R18)
            : base(Downloader)
        {
            _Domain = R18 ? "com" : "net";
            _Config = new APIConfig()
            {
                Name = "Konachan",
                Secure = true
                // no login support
            };
            _URLRegex = new Regex(@"(https?:\/\/|)(www.|)konachan." + _Domain + @"\/post\/show\/[0-9]+(\/.*|)");
            _IDRegex = new Regex("show\\/[0-9]+");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            var str = _IDRegex.Match(URL).Value.Substring(5);
            return Convert.ToUInt32(str);
        }

        public override APIPost GetPost(uint ID)
        {
            XmlDocument document = Downloader.DownloadXML("https://konachan." + _Domain + "/post.xml?tags=id%3A" + ID);
            XmlNodeList xmlposts = document["posts"].GetElementsByTagName("post");
            if (xmlposts.Count > 0)
            {
                XmlAttributeCollection attribs = xmlposts[0].Attributes;
                return new APIPost()
                {
                    Source = "https://konachan.com/post/show/" + ID,
                    Tags = attribs["tags"].Value.Split(' '),
                    ThumbnailURL = "https:" + attribs["preview_url"].Value,
                    // SampleURL = "https:" + attribs["sample_url"].Value,
                    ImageURL = "https:" + attribs["file_url"].Value,
                    Rating = APIPost.ParseRatingChar(attribs["rating"].Value[0])
                };
            }
            else throw new Exception("post not found");
        }
    }
}
