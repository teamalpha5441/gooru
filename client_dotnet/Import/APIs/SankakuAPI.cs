﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class SankakuAPI : API
    {
        // Sometimes Sankaku returns some sort of hotlink protection image for ImageURL
        // sha256 of this image: f342845116cba1bf1f182e2b61e7ef86ca53cfe16f28a5c66da6243262ec5d06

        // only four letters per type allowed (because of regex patterns)
        public enum SankakuType { Chan, Idol }

        private string _SubDomain;
        private string _ThumbSubDomain;
        private string _ImageSubDomain;
        private APIConfig _Config;
        private Regex _URLRegex;
        private Regex _IDRegex;
        private Regex _ImageRegex;
        private Regex _RatingRegex;
        private Regex _TagRegex;

        public override APIConfig Config { get { return _Config; } }

        public SankakuAPI(Downloader Downloader, SankakuType Type)
            : base(Downloader)
        {
            // works at least for chan and idol, but maybe breaks if more types are added
            _SubDomain = Enum.GetName(typeof(SankakuType), Type).ToLower();
            _ThumbSubDomain = _SubDomain[0] + ""; // "c" for chan, "i" for idol
            _ImageSubDomain = _ThumbSubDomain + "s"; // "cs" for chan, "is" for idol
            _Config = new APIConfig()
            {
                Name = "Sankaku",
                Secure = true,
                // no login support
            };
            _URLRegex = new Regex(@"(https?:\/\/|)" + _SubDomain + @".sankakucomplex.com\/post\/show\/[0-9]+");
            _IDRegex = new Regex("show\\/[0-9]+");
            _ImageRegex = new Regex(@"\/\/" + _ImageSubDomain + @".sankakucomplex.com\/data\/[a-f0-9]{2}\/[a-f0-9]{2}\/[a-f0-9]{32}.[a-z0-9]{3,4}\?.*");
            _RatingRegex = new Regex("Rating: (Safe|Questionable|Explicit)");
            _TagRegex = new Regex("\\/\\?tags=.+?\"");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            var str = _IDRegex.Match(URL).Value.Substring(5);
            return Convert.ToUInt32(str);
        }

        public override APIPost GetPost(uint ID)
        {
            string document = Downloader.DownloadString("https://" + _SubDomain + ".sankakucomplex.com/post/show/" + ID);
            var post = new APIPost();

            // extract original picture link
            int index = document.IndexOf("Original: <a");
            if (index < 0)
                throw new Exception("start of original picture link not found");
            index = document.IndexOf("href=\"//" + _ImageSubDomain + ".sankaku", index + 13);
            if (index < 0)
                throw new Exception("start of original picture link href not found");
            int linkStart = index + 6;
            index = document.IndexOf('"', linkStart);
            if (index < 0)
                throw new Exception("end of original picture link href not found");
            string link = document.Substring(linkStart, index - linkStart);
            if (!_ImageRegex.IsMatch(link))
                throw new Exception("original picture link not matched");
            // append https at the beginning and cut off question mark and id at the end
            post.ImageURL = "https:" + link.Substring(0, link.IndexOf('?', 32));
            // extract hash and generate thumbnail url
            string hash = link.Substring(_ImageSubDomain.Length + 33, 32);
            post.ThumbnailURL = string.Format("https://{0}.sankakucomplex.com/data/preview/{1}/{2}/{3}.jpg", _ThumbSubDomain, hash.Substring(0, 2), hash.Substring(2, 2), hash);

            // extract rating
            var ratingMatch = _RatingRegex.Match(document);
            if (!ratingMatch.Success)
                throw new Exception("rating string not found or not matching");
            post.Rating = APIPost.ParseRatingChar(char.ToLower(ratingMatch.Value[8]));

            // find tag sidebar
            index = document.IndexOf("<ul id=tag-sidebar>");
            if (index < 0)
                throw new Exception("start of tag sidebar not found");
            int tagsStart = index + 19;
            index = document.IndexOf("</ul>", tagsStart);
            if (index < 0)
                throw new Exception("end of tag sidebar not found");
            string tagDocument = document.Substring(tagsStart, index - tagsStart);

            // extract tags
            var tags = new List<string>();
            foreach (Match m in _TagRegex.Matches(tagDocument))
            {
                // value looks like this: /?tags=some_tag"
                // now extract the actual tag: some_tag
                string tag = m.Value.Substring(7, m.Value.Length - 8);
                tags.Add(Uri.UnescapeDataString(tag));
            }
            post.Tags = tags.ToArray();

            // create source link and return post
            post.Source = "https://" + _SubDomain + ".sankakucomplex.com/post/show/" + ID;
            return post;
        }
    }
}
