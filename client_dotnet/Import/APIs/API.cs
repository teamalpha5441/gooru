﻿namespace teamalpha5441.Booru.Import.APIs
{
    public abstract class API
    {
        public struct APIConfig
        {
            public string Name;
            public bool Secure;
            public string LoginURL;
            public string UsernameKey;
            public string PasswordKey;
        }

        public abstract APIConfig Config { get; }
        protected Downloader Downloader { get; private set; }

        private bool _LoggedIn;

        protected API(Downloader Downloader)
        {
            this.Downloader = Downloader;
            this._LoggedIn = false;
        }

        public void Login(APILogin Login, bool Force = false)
        {
            if (!_LoggedIn || Force)
                Downloader.Login(Config.LoginURL, Config.UsernameKey, Login.Username, Config.PasswordKey, Login.Password);
            _LoggedIn = true;
        }

        public abstract uint? GetIDFromURL(string URL);
        public abstract APIPost GetPost(uint ID);
    }
}
