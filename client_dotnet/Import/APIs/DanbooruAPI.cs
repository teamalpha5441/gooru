using System;
using System.Xml;
using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class DanbooruAPI : API
    {
        private APIConfig _Config;
        private Regex _URLRegex;
        private Regex _IDRegex;

        public override APIConfig Config { get { return _Config; } }

        public DanbooruAPI(Downloader Downloader)
            : base(Downloader)
        {
            _Config = new APIConfig()
            {
                Name = "Danbooru",
                Secure = true,
                LoginURL = "https://danbooru.donmai.us/session",
                UsernameKey = "name",
                PasswordKey = "password"
            };
            _URLRegex = new Regex(@"(https?:\/\/|)(www.|)danbooru.donmai.us\/posts\/[0-9]+.*");
            _IDRegex = new Regex("posts\\/[0-9]+");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            var str = _IDRegex.Match(URL).Value.Substring(6);
            return Convert.ToUInt32(str);
        }

        public override APIPost GetPost(uint ID)
        {
            XmlDocument document = Downloader.DownloadXML("https://danbooru.donmai.us/posts.xml?tags=id%3A" + ID);
            XmlNodeList xmlposts = document["posts"].GetElementsByTagName("post");
            if (xmlposts.Count > 0)
            {
                XmlNode post = xmlposts[0];
                return new APIPost()
                {
                    Source = "https://danbooru.donmai.us/posts/" + ID,
                    Tags = post["tag-string"].InnerText.Split(' '),
                    ThumbnailURL = "https://danbooru.donmai.us" + post["preview-file-url"].InnerText,
                    // wtf danbooru? file-url = image and large-file-url = sample ??
                    // SampleURL = "https://danbooru.donmai.us" + post["large-file-url"].InnerText,
                    ImageURL = "https://danbooru.donmai.us" + post["file-url"].InnerText,
                    Rating = APIPost.ParseRatingChar(post["rating"].InnerText[0])
                };
            }
            else throw new Exception("post not found");
        }
    }
}
