using System;
using System.Xml;
using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class GelbooruAPI : API
    {
        private APIConfig _Config;
        private Regex _URLRegex;
        private Regex _IDRegex;

        public override APIConfig Config { get { return _Config; } }

        public GelbooruAPI(Downloader Downloader)
            : base(Downloader)
        {
            _Config = new APIConfig()
            {
                Name = "Gelbooru",
                Secure = true,
                LoginURL = "https://gelbooru.com/index.php?page=account&s=login&code=00",
                UsernameKey = "user",
                PasswordKey = "pass"
            };
            _URLRegex = new Regex(@"(https?:\/\/|)(www.|)gelbooru.com\/index.php\?page=post&s=view&id=[0-9]+(&.+|)");
            _IDRegex = new Regex("&id=[0-9]+");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            var str = _IDRegex.Match(URL).Value.Substring(4);
            return Convert.ToUInt32(str);
        }

        public override APIPost GetPost(uint ID)
        {
            XmlDocument document = Downloader.DownloadXML("https://gelbooru.com/index.php?page=dapi&s=post&q=index&id=" + ID);
            XmlNodeList xmlposts = document["posts"].GetElementsByTagName("post");
            if (xmlposts.Count > 0)
            {
                XmlAttributeCollection attribs = xmlposts[0].Attributes;
                return new APIPost()
                {
                    Source = "https://gelbooru.com/index.php?page=post&s=view&id=" + ID,
                    Tags = attribs["tags"].Value.Split(' '),
                    ThumbnailURL = attribs["preview_url"].Value,
                    // SampleURL = attribs["sample_url"].Value,
                    ImageURL = attribs["file_url"].Value,
                    Rating = APIPost.ParseRatingChar(attribs["rating"].Value[0])
                };
            }
            else throw new Exception("post not found");
        }
    }
}
