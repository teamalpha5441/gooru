using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class Pr0grammAPI : API
    {
        private const ushort FLAG_SFW = 1;
        private const ushort FLAG_NSFW = 2;
        private const ushort FLAG_NSFL = 4;
        private const ushort FLAG_NSFP = 8;
        private const ushort FLAG_ALL = 15;

#pragma warning disable 0649

        private struct GetResponse
        {
            public GetResponseItem[] items;
        }

        private struct GetResponseItem
        {
            public uint id;
            public string image, thumb;
            public ushort flags;
        }

        private struct InfoResponse
        {
            public InfoResponseTag[] tags;
        }

        private struct InfoResponseTag
        {
            public string tag;
        }

#pragma warning restore 0649

        private APIConfig _Config;
        private Regex _URLRegex;

        public override APIConfig Config { get { return _Config; } }

        public Pr0grammAPI(Downloader Downloader)
            : base(Downloader)
        {
            string userRegex = @"user\/.+\/(uploads|likes)\/";
            string searchRegex = @"(new|top)\/(.+\/|)";
            _Config = new APIConfig()
            {
                Name = "Pr0gramm",
                Secure = true,
                LoginURL = "https://pr0gramm.com/api/user/login",
                UsernameKey = "name",
                PasswordKey = "password"
            };
            _URLRegex = new Regex(@"(https?:\/\/|)(www.|)pr0gramm.com\/(" + searchRegex + "|" + userRegex + ")[0-9]+");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            int lastSlash = URL.LastIndexOf('/');
            return Convert.ToUInt32(URL.Substring(lastSlash + 1));
        }

        public override APIPost GetPost(uint ID)
        {
            // request item(s)
            string getURL = string.Format("https://pr0gramm.com/api/items/get?id={0}&flags={1}", ID, FLAG_ALL);
            string getResponseString = Downloader.DownloadString(getURL);
            var getResponse = JsonConvert.DeserializeObject<GetResponse>(getResponseString);
            var post = new APIPost();

            // get item
            GetResponseItem? item = null;
            foreach (var getItem in getResponse.items)
                if (getItem.id == ID)
                {
                    item = getItem;
                    break;
                }
            if (!item.HasValue)
                throw new Exception("couldn't get post");

            // extract and set rating and image, thumb and source urls
            bool isVideo = item.Value.image.EndsWith(".webm") || item.Value.image.EndsWith(".mp4");
            post.ThumbnailURL = "https://thumb.pr0gramm.com/" + item.Value.thumb;
            post.ImageURL = "https://" + (isVideo ? "vid" : "img") + ".pr0gramm.com/" + item.Value.image;
            post.Source = "https://pr0gramm.com/new/" + ID;
            ushort flags = item.Value.flags;
            if ((flags & FLAG_NSFW) > 0 || (flags & FLAG_NSFL) > 0)
                post.Rating = APIRating.Explicit;
            else post.Rating = APIRating.Safe;

            // request info
            string infoURL = "https://pr0gramm.com/api/items/info?itemId=" + ID;
            string infoResponseString = Downloader.DownloadString(infoURL);
            var infoResponse = JsonConvert.DeserializeObject<InfoResponse>(infoResponseString);

            // extract and set tags
            post.Tags = new string[infoResponse.tags.Length];
            for (int i = 0; i < infoResponse.tags.Length; i++)
                post.Tags[i] = infoResponse.tags[i].tag.ToLower().Replace(' ', '_');

            return post;
        }
    }
}
