using System;
using System.Xml;
using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import.APIs
{
    public sealed class YandereAPI : API
    {
        // YandereAPI isn't supported in Mono because Mono doesn't support DH key exchange cipher suites
        // Server would need to include for example TLS_RSA_WITH_AES_256_CBC_SHA for Mono support

        private APIConfig _Config;
        private Regex _URLRegex;
        private Regex _IDRegex;

        public override APIConfig Config { get { return _Config; } }

        public YandereAPI(Downloader Downloader)
            : base(Downloader)
        {
            _Config = new APIConfig()
            {
                Name = "Yandere",
                Secure = true,
                LoginURL = "https://yande.re/user/authenticate",
                UsernameKey = "user[name]",
                PasswordKey = "user[password]"
            };
            _URLRegex = new Regex(@"(https?:\/\/|)(www.|)yande.re\/post\/show\/[0-9]+(\/.*|)");
            _IDRegex = new Regex("show\\/[0-9]+");
        }

        public override uint? GetIDFromURL(string URL)
        {
            if (!_URLRegex.IsMatch(URL))
                return null;
            var str = _IDRegex.Match(URL).Value.Substring(5);
            return Convert.ToUInt32(str);
        }

        public override APIPost GetPost(uint ID)
        {
            XmlDocument document = Downloader.DownloadXML("https://yande.re/post.xml?tags=id%3A" + ID);
            XmlNodeList xmlposts = document["posts"].GetElementsByTagName("post");
            if (xmlposts.Count > 0)
            {
                XmlAttributeCollection attribs = xmlposts[0].Attributes;
                return new APIPost()
                {
                    Source = "https://yande.re/post/show/" + ID,
                    Tags = attribs["tags"].Value.Split(' '),
                    ThumbnailURL = attribs["preview_url"].Value,
                    // SampleURL = attribs["sample_url"].Value,
                    ImageURL = attribs["file_url"].Value,
                    Rating = APIPost.ParseRatingChar(attribs["rating"].Value[0])
                };
            }
            else throw new Exception("post not found");
        }
    }
}
