using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;

namespace teamalpha5441.Booru.Import
{
    public class Downloader
    {
        private WebProxy _Proxy;
        private CookieContainer _Cookies;

        public Downloader(WebProxy Proxy = null)
        {
            this._Proxy = Proxy;
            this._Cookies = new CookieContainer();
        }

        private HttpWebResponse DoRequest(string URL, string Method, string Content = null)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL);
            if (_Proxy != null)
                request.Proxy = _Proxy;
            request.CookieContainer = _Cookies;

            // BehoimiAPI will return 403 if no (valid) UserAgent is sent
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:400.0) Gecko/20202020 Firefox/400.0";

            // BehoimiAPI will return an image of sausages if referer URL is not valid
            // setting the referer URL to request URL works
            request.Referer = URL;

            request.Method = Method;
            if (Content != null)
            {
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] requestData = Encoding.UTF8.GetBytes(Content);
                using (Stream requestStream = request.GetRequestStream())
                    requestStream.Write(requestData, 0, requestData.Length);
            }

            return (HttpWebResponse)request.GetResponse();
        }

        public byte[] DownloadData(string URL)
        {
            HttpWebResponse response = DoRequest(URL, "GET");
            using (MemoryStream ms = new MemoryStream())
            {
                using (Stream responseStream = response.GetResponseStream())
                    responseStream.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public XmlDocument DownloadXML(string URL)
        {
            XmlDocument document = new XmlDocument();
            HttpWebResponse response = DoRequest(URL, "GET");
            using (Stream responseStream = response.GetResponseStream())
                document.Load(responseStream);
            return document;
        }

        public string DownloadString(string URL) { return Encoding.UTF8.GetString(DownloadData(URL)); }

        public void Login(string URL, string UsernameKey, string Username, string PasswordKey, string Password)
        {
            Username = Uri.EscapeDataString(Username);
            Password = Uri.EscapeDataString(Password);
            string content = string.Format("{0}={1}&{2}={3}", UsernameKey, Username, PasswordKey, Password);
            var response = DoRequest(URL, "POST", content);
            HttpStatusCode statusCode = response.StatusCode;
            response.Close();
            if (statusCode != HttpStatusCode.OK)
                throw new Exception("Login failed (" + Enum.GetName(typeof(HttpStatusCode), statusCode) + ")");
        }
    }
}
