using System;
using System.Net;
using System.Xml;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using teamalpha5441.Booru.Import.APIs;

namespace teamalpha5441.Booru.Import
{
    public sealed class Importer
    {
        public readonly Downloader Downloader;
        private Dictionary<string, APILogin> _Logins;
        private API[] _APIs;
        private Regex _RegexNameIDPair;

        public Importer(Dictionary<string, APILogin> Logins)
        {
            Downloader = new Downloader();
            _Logins = Logins;
            _APIs = new API[]
            {
                new BehoimiAPI(Downloader),
                new DanbooruAPI(Downloader),
                new GelbooruAPI(Downloader),
                new KonachanAPI(Downloader, false),
                new KonachanAPI(Downloader, true),
                new YandereAPI(Downloader),
                // new SankakuAPI(Downloader, SankakuAPI.SankakuType.Chan),
                // new SankakuAPI(Downloader, SankakuAPI.SankakuType.Idol),
                new Pr0grammAPI(Downloader)
            };
            _RegexNameIDPair = new Regex("[A-Za-z0-9]+#[0-9]+");
        }

        private bool GetAPIAndID(string URL, ref API API, ref uint ID)
        {
            if (_RegexNameIDPair.IsMatch(URL))
            {
                string wantedName = URL.Substring(0, URL.IndexOf('#'));
                foreach (API api in _APIs)
                    if (string.Compare(api.Config.Name, wantedName, true) == 0)
                    {
                        API = api;
                        string idString = URL.Substring(wantedName.Length + 1);
                        ID = Convert.ToUInt32(idString);
                        return true;
                    }
            }
            else foreach (API api in _APIs)
                {
                    uint? id = api.GetIDFromURL(URL);
                    if (id.HasValue)
                    {
                        API = api;
                        ID = id.Value;
                        return true;
                    }
                }
            return false;
        }

        public APIPost GetPost(string URL)
        {
            API api = null;
            uint id = 0;
            if (!GetAPIAndID(URL, ref api, ref id))
                return null;

            if (api.Config.LoginURL != null && _Logins.ContainsKey(api.Config.Name))
            {
                // skip authentication if the API doesn't support TLS
                //TODO add switch to override auth skipping
                if (api.Config.Secure)
                    api.Login(_Logins[api.Config.Name]);
                else Console.Error.WriteLine("Importer API doesn't support TLS, skipping login request.");
            }
            return api.GetPost(id);
        }
    }
}
