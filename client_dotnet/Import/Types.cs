﻿using System.Text.RegularExpressions;

namespace teamalpha5441.Booru.Import
{
    public sealed class APILogin
    {
        public string Username;
        public string Password;
    }

    public enum APIRating
    {
        Safe = 1,
        Questionable = 2,
        Explicit = 3
    }

    public sealed class APIPost
    {
        public string Source;
        public string[] Tags;
        public string ThumbnailURL;
        public string ImageURL;
        public APIRating Rating;

        public static APIRating ParseRatingChar(char RC)
        {
            if (RC == 's')
                return APIRating.Safe;
            else if (RC == 'q')
                return APIRating.Questionable;
            return APIRating.Explicit;
        }
    }
}
