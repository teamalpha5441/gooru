﻿namespace teamalpha5441.Booru.Client
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.textBoxTags = new System.Windows.Forms.TextBox();
            this.textBoxSource = new System.Windows.Forms.TextBox();
            this.labelTags = new System.Windows.Forms.Label();
            this.labelSource = new System.Windows.Forms.Label();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.radioButtonRating1 = new System.Windows.Forms.RadioButton();
            this.radioButtonRating2 = new System.Windows.Forms.RadioButton();
            this.radioButtonRating3 = new System.Windows.Forms.RadioButton();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.checkBoxPrivate = new System.Windows.Forms.CheckBox();
            this.checkBoxForce = new System.Windows.Forms.CheckBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuItemImportFromClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLoadImage = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(12, 27);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(358, 358);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // textBoxTags
            // 
            this.textBoxTags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTags.Location = new System.Drawing.Point(376, 43);
            this.textBoxTags.Multiline = true;
            this.textBoxTags.Name = "textBoxTags";
            this.textBoxTags.Size = new System.Drawing.Size(336, 221);
            this.textBoxTags.TabIndex = 1;
            // 
            // textBoxSource
            // 
            this.textBoxSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSource.Location = new System.Drawing.Point(423, 270);
            this.textBoxSource.Name = "textBoxSource";
            this.textBoxSource.Size = new System.Drawing.Size(289, 20);
            this.textBoxSource.TabIndex = 2;
            // 
            // labelTags
            // 
            this.labelTags.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTags.AutoSize = true;
            this.labelTags.Location = new System.Drawing.Point(376, 27);
            this.labelTags.Name = "labelTags";
            this.labelTags.Size = new System.Drawing.Size(31, 13);
            this.labelTags.TabIndex = 3;
            this.labelTags.Text = "Tags";
            // 
            // labelSource
            // 
            this.labelSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSource.AutoSize = true;
            this.labelSource.Location = new System.Drawing.Point(376, 273);
            this.labelSource.Name = "labelSource";
            this.labelSource.Size = new System.Drawing.Size(41, 13);
            this.labelSource.TabIndex = 4;
            this.labelSource.Text = "Source";
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInfo.Location = new System.Drawing.Point(423, 296);
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.Size = new System.Drawing.Size(289, 20);
            this.textBoxInfo.TabIndex = 5;
            // 
            // labelInfo
            // 
            this.labelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelInfo.AutoSize = true;
            this.labelInfo.Location = new System.Drawing.Point(376, 299);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(25, 13);
            this.labelInfo.TabIndex = 6;
            this.labelInfo.Text = "Info";
            // 
            // radioButtonRating1
            // 
            this.radioButtonRating1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonRating1.AutoSize = true;
            this.radioButtonRating1.Location = new System.Drawing.Point(376, 322);
            this.radioButtonRating1.Name = "radioButtonRating1";
            this.radioButtonRating1.Size = new System.Drawing.Size(47, 17);
            this.radioButtonRating1.TabIndex = 7;
            this.radioButtonRating1.Text = "Safe";
            this.radioButtonRating1.UseVisualStyleBackColor = true;
            // 
            // radioButtonRating2
            // 
            this.radioButtonRating2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonRating2.AutoSize = true;
            this.radioButtonRating2.Location = new System.Drawing.Point(376, 345);
            this.radioButtonRating2.Name = "radioButtonRating2";
            this.radioButtonRating2.Size = new System.Drawing.Size(87, 17);
            this.radioButtonRating2.TabIndex = 8;
            this.radioButtonRating2.Text = "Questionable";
            this.radioButtonRating2.UseVisualStyleBackColor = true;
            // 
            // radioButtonRating3
            // 
            this.radioButtonRating3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonRating3.AutoSize = true;
            this.radioButtonRating3.Checked = true;
            this.radioButtonRating3.Location = new System.Drawing.Point(376, 368);
            this.radioButtonRating3.Name = "radioButtonRating3";
            this.radioButtonRating3.Size = new System.Drawing.Size(58, 17);
            this.radioButtonRating3.TabIndex = 9;
            this.radioButtonRating3.TabStop = true;
            this.radioButtonRating3.Text = "Explicit";
            this.radioButtonRating3.UseVisualStyleBackColor = true;
            // 
            // buttonUpload
            // 
            this.buttonUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpload.Location = new System.Drawing.Point(534, 322);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(178, 63);
            this.buttonUpload.TabIndex = 10;
            this.buttonUpload.Text = "Upload";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // checkBoxPrivate
            // 
            this.checkBoxPrivate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxPrivate.AutoSize = true;
            this.checkBoxPrivate.Location = new System.Drawing.Point(469, 323);
            this.checkBoxPrivate.Name = "checkBoxPrivate";
            this.checkBoxPrivate.Size = new System.Drawing.Size(59, 17);
            this.checkBoxPrivate.TabIndex = 11;
            this.checkBoxPrivate.Text = "Private";
            this.checkBoxPrivate.UseVisualStyleBackColor = true;
            // 
            // checkBoxForce
            // 
            this.checkBoxForce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxForce.AutoSize = true;
            this.checkBoxForce.Location = new System.Drawing.Point(469, 346);
            this.checkBoxForce.Name = "checkBoxForce";
            this.checkBoxForce.Size = new System.Drawing.Size(53, 17);
            this.checkBoxForce.TabIndex = 12;
            this.checkBoxForce.Text = "Force";
            this.checkBoxForce.UseVisualStyleBackColor = true;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemImportFromClipboard,
            this.menuItemLoadImage});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(724, 24);
            this.menuStrip.TabIndex = 13;
            this.menuStrip.Text = "menuStrip1";
            // 
            // menuItemImportFromClipboard
            // 
            this.menuItemImportFromClipboard.Name = "menuItemImportFromClipboard";
            this.menuItemImportFromClipboard.Size = new System.Drawing.Size(176, 20);
            this.menuItemImportFromClipboard.Text = "Import from URL in Clipboard";
            this.menuItemImportFromClipboard.Click += new System.EventHandler(this.menuItemImportFromClipboard_Click);
            // 
            // menuItemLoadImage
            // 
            this.menuItemLoadImage.Name = "menuItemLoadImage";
            this.menuItemLoadImage.Size = new System.Drawing.Size(81, 20);
            this.menuItemLoadImage.Text = "Load Image";
            this.menuItemLoadImage.Click += new System.EventHandler(this.menuItemLoadImage_Click);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 397);
            this.Controls.Add(this.checkBoxForce);
            this.Controls.Add(this.checkBoxPrivate);
            this.Controls.Add(this.buttonUpload);
            this.Controls.Add(this.radioButtonRating3);
            this.Controls.Add(this.radioButtonRating2);
            this.Controls.Add(this.radioButtonRating1);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.labelSource);
            this.Controls.Add(this.labelTags);
            this.Controls.Add(this.textBoxSource);
            this.Controls.Add(this.textBoxTags);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip);
            this.Icon = global::teamalpha5441.Booru.Client.Properties.Resources.icon;
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(740, 435);
            this.Name = "GUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Booru Uploader";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox textBoxTags;
        private System.Windows.Forms.TextBox textBoxSource;
        private System.Windows.Forms.Label labelTags;
        private System.Windows.Forms.Label labelSource;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.RadioButton radioButtonRating1;
        private System.Windows.Forms.RadioButton radioButtonRating2;
        private System.Windows.Forms.RadioButton radioButtonRating3;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.CheckBox checkBoxPrivate;
        private System.Windows.Forms.CheckBox checkBoxForce;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuItemImportFromClipboard;
        private System.Windows.Forms.ToolStripMenuItem menuItemLoadImage;
    }
}