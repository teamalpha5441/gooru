using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Security.Cryptography;
using teamalpha5441.Booru.Import;
using CommandLine;

namespace teamalpha5441.Booru.Client
{
    public sealed class CLI
    {
        private BooruClient _BooruClient;
        private Importer _Importer;

        public CLI(BooruClient BooruClient, Importer Importer)
        {
            _BooruClient = BooruClient;
            _Importer = Importer;
        }

        private byte[] LoadImage(string Path)
        {
            Console.Write("Loading image... ");
            MemoryStream img = new MemoryStream();
            using (FileStream fs = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.Read))
                fs.CopyTo(img);
            Console.WriteLine("{0:0.00}MB", img.Length / 1048576f);
            return img.ToArray();
        }

        private byte[] DownloadImage(string URL)
        {
            Console.Write("Downloading image... ");
            byte[] img = _Importer.Downloader.DownloadData(URL);
            Console.WriteLine("{0:0.00}MB", img.Length / 1048576f);
            return img;
        }

        private string[] SplitTags(string TagString)
        {
            var splitChars = new char[4] { ' ', '\t', '\r', '\n' };
            return TagString.ToLower().Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
        }

        private void TagDelta(ref List<string> Tags, string deltaString)
        {
            string[] deltaParts = SplitTags(deltaString);
            foreach (string part in deltaParts)
                if ((part.StartsWith("!") || part.StartsWith("_")) && part.Length > 1)
                {
                    string subPart = part.Substring(1);
                    Tags.RemoveAll(str => str == subPart);
                }
                else Tags.Add(part);
        }

        public bool Run(string verb, object verbOptions)
        {
            switch (verb)
            {
                case Options.VERB_ADD: RunAdd((AddOptions)verbOptions); return true;
                case Options.VERB_IMPORT: RunImport((ImportOptions)verbOptions); return true;
                case Options.VERB_EDIT: RunEdit((EditOptions)verbOptions); return true;
                case Options.VERB_SETIMG: RunSetImg((SetImgOptions)verbOptions); return true;
                case Options.VERB_DEL:
                    var options = (DelOptions)verbOptions;
                    _BooruClient.DeletePost(options.ID);
                    return true;
            }
            return false;
        }

        private void RunAdd(AddOptions options)
        {
            byte[] image = LoadImage(options.ImagePath);
            var tags = SplitTags(options.Tags);
            byte rating = (byte)options.Rating;
            Console.Write("Uploading post... ");
            uint id = _BooruClient.UploadPost(image.ToArray(), options.Private, options.Source, options.Info, rating, tags, options.Force);
            Console.WriteLine(id);
        }

        private void RunImport(ImportOptions options)
        {
            var apiPost = _Importer.GetPost(options.URL);
            byte[] image = DownloadImage(apiPost.ImageURL);
            string info = null;
            if (options.Info != null)
                info = options.Info;
            byte rating = (byte)options.Rating;
            var tags = new List<string>();
            if (!options.AllTags)
            {
                Console.Write("Checking tag or alias existence... ");
                var existing = _BooruClient.GetAllTagsAndAliases();
                foreach (string tag in apiPost.Tags)
                    if (existing.Contains(tag))
                        tags.Add(tag);
                Console.WriteLine("OK");
            }
            else tags.AddRange(apiPost.Tags);
            if (options.Tags != null)
            {
                options.Tags = options.Tags.ToLower();
                if (options.TagsNoDelta)
                    tags = SplitTags(options.Tags).ToList();
                else TagDelta(ref tags, options.Tags);
            }
            Console.Write("Uploading post... ");
            ulong id = _BooruClient.UploadPost(image, options.Private, apiPost.Source, info, rating, tags.ToArray(), options.Force);
            Console.WriteLine(id);
        }

        private void RunEdit(EditOptions options)
        {
            var post = _BooruClient.GetPost(options.ID);
            if (options.Info != null)
                post.Info = options.Info;
            if (options.Private.HasValue)
                post.Private = options.Private.Value;
            if (options.Rating >= 0)
                post.Rating = (byte)options.Rating;
            if (options.Source != null)
                post.Source = options.Source;
            string[] tags = null;
            if (options.Tags != null)
            {
                if (!options.TagsNoDelta)
                {
                    var currentTags = new List<string>();
                    foreach (var tag in post.Tags)
                        currentTags.Add(tag.Tag);
                    TagDelta(ref currentTags, options.Tags);
                    tags = currentTags.ToArray();
                }
                else tags = SplitTags(options.Tags);
            }
            _BooruClient.UpdatePost(post, tags);
        }

        private void RunSetImg(SetImgOptions options)
        {
            byte[] image = LoadImage(options.ImagePath);
            Console.Write("Uploading image... ");
            _BooruClient.SetImage(options.ID, image);
            Console.WriteLine("OK");
        }
    }
}
