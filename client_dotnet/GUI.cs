﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using teamalpha5441.Booru.Import;

namespace teamalpha5441.Booru.Client
{
    public partial class GUI : Form
    {
        private BooruClient _BooruClient;
        private Importer _Importer;

        private byte _UIRating = 3;
        private byte[] _CurrentImage;

        // property 'methods' may throw exceptions

        private byte UIRating
        {
            get { return _UIRating; }
            set
            {
                if (value > 0 && value <= 3)
                    _UIRating = value;
                else _UIRating = 3;
                radioButtonRating1.Checked = false;
                radioButtonRating2.Checked = false;
                radioButtonRating3.Checked = false;
                switch (_UIRating)
                {
                    case 1: radioButtonRating1.Checked = true; break;
                    case 2: radioButtonRating2.Checked = true; break;
                    case 3: radioButtonRating3.Checked = true; break;
                }
            }
        }

        private APIPost CurrentPost
        {
            set
            {
                if (pictureBox.Image != null)
                    pictureBox.Image.Dispose();
                pictureBox.Image = null;
                textBoxTags.Clear();
                textBoxInfo.Clear();
                checkBoxPrivate.Checked = false;
                checkBoxForce.Checked = false;
                if (value == null)
                {
                    _CurrentImage = null;
                    textBoxSource.Clear();
                    UIRating = 0;
                }
                else
                {
                    // download and set image
                    try { _CurrentImage = _Importer.Downloader.DownloadData(value.ImageURL); }
                    catch (Exception ex) { MessageBox.Show("Image download failed!" + Environment.NewLine + "Error: " + ex.Message, "Booru Download Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                    // download and set thumbnail
                    if (_CurrentImage != null)
                        setImage();

                    // download all tags and aliases
                    string[] all = _BooruClient.GetAllTagsAndAliases();
                    for (int i = 0; i < value.Tags.Length; i++)
                        if (all.Contains(value.Tags[i]))
                            textBoxTags.AppendText((i > 0 ? " " : "") + value.Tags[i]);

                    textBoxSource.Text = value.Source;
                    UIRating = (byte)value.Rating;
                }
            }
        }

        public GUI(BooruClient BooruClient, Importer Importer)
        {
            InitializeComponent();

            _BooruClient = BooruClient;
            _Importer = Importer;
        }

        // helper methods show message boxes

        private void setImage()
        {
            if (pictureBox.Image != null)
                pictureBox.Image.Dispose();
            using (MemoryStream ms = new MemoryStream(_CurrentImage))
                try
                {
                    Bitmap b = new Bitmap(ms);
                    pictureBox.Image = b;
                }
                catch (Exception ex)
                {
                    pictureBox.Image = null;
                    string msg = "Image downloaded successfully, but it couldn't be shown (maybe it's a video?)." + Environment.NewLine + "Error: ";
                    MessageBox.Show(msg + ex.Message, "Booru Image Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
        }

        // handler methods show message boxes

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            try
            {
                string[] tags = textBoxTags.Text.ToLower().Split(new char[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                uint pid = _BooruClient.UploadPost(_CurrentImage, checkBoxPrivate.Checked, textBoxSource.Text, textBoxInfo.Text, UIRating, tags, checkBoxForce.Checked);
                MessageBox.Show("Upload finished successfully! PID #" + pid, "Booru Upload", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CurrentPost = null;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Booru Upload Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void menuItemImportFromClipboard_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Clipboard.ContainsText())
                    throw new Exception("Clipboard doesn't contain text");
                var post = _Importer.GetPost(Clipboard.GetText());
                if (post == null)
                    throw new Exception("Clipboard doesn't contain a valid Booru URL");
                CurrentPost = post;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Booru Download Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void menuItemLoadImage_Click(object sender, EventArgs e)
        {
            string path = null;
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Open image or video ...";
                ofd.Filter = "Img/Vid|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                    path = ofd.FileName;
            }
            if (path != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                        fs.CopyTo(ms);
                    _CurrentImage = ms.ToArray();
                }
                setImage();
            }
        }
    }
}
