﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Booru .net Client")]
[assembly: AssemblyDescription("Booru .net Client (CLI and GUI)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("teamalpha5441")]
[assembly: AssemblyProduct("Booru .net Client")]
[assembly: AssemblyCopyright("© by teamalpha5441 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
