using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace teamalpha5441.Booru.Client
{
    public class Config
    {
        public struct Login
        {
            public string Username, Password;
        }

#pragma warning disable 0649

        private struct jsonConfig
        {
            public jsonBooruConfig Booru;
            public jsonImportLogin[] Import;
        }

        private struct jsonBooruConfig
        {
            public string URL;
            public string Username, Password;
        }

        private struct jsonImportLogin
        {
            public string[] APIs;
            public string Username, Password;
        }

#pragma warning restore 0649

        private const string CONFIG_FILE_NAME = "booru.json";

        public string BooruURL {get;private set;}
        public Login BooruLogin { get; private set; }
        public Dictionary<string, Login> ImportLogins { get; private set; }

        public static Config Load()
        {
            string[] paths = new string[] {
                CONFIG_FILE_NAME, // current working directory
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), CONFIG_FILE_NAME)
            };

            for (int i = 0; i < paths.Length; i++)
                if (File.Exists(paths[i]))
                    using (FileStream fileStream = new FileStream(paths[i], FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (StreamReader fileReader = new StreamReader(fileStream))
                    using (JsonReader jsonReader = new JsonTextReader(fileReader))
                    {
                        var jsonConfig = (new JsonSerializer()).Deserialize<jsonConfig>(jsonReader);
                        var config = new Config()
                        {
                            BooruURL = jsonConfig.Booru.URL,
                            BooruLogin = new Login()
                            {
                                Username = jsonConfig.Booru.Username,
                                Password = jsonConfig.Booru.Password
                            },
                            ImportLogins = new Dictionary<string, Login>(16)
                        };
                        foreach (var jsonImportLogin in jsonConfig.Import)
                            foreach (string api in jsonImportLogin.APIs)
                                config.ImportLogins.Add(api, new Login()
                                {
                                    Username = jsonImportLogin.Username,
                                    Password = jsonImportLogin.Password
                                });
                        return config;
                    }

            throw new FileNotFoundException("Config file not found (" + CONFIG_FILE_NAME + ")");
        }
    }
}
