using CommandLine;
using CommandLine.Text;

namespace teamalpha5441.Booru.Client
{
    public class Options
    {
        public const string VERB_ADD = "add";
        public const string VERB_IMPORT = "import";
        public const string VERB_DEL = "del";
        public const string VERB_EDIT = "edit";
        public const string VERB_SETIMG = "setimg";
        public const string VERB_GUI = "gui";

        public Options()
        {
            AddVerb = new AddOptions();
            ImportVerb = new ImportOptions();
            DelVerb = new DelOptions();
            EditVerb = new EditOptions();
            SetImgVerb = new SetImgOptions();
            GUIVerb = new GUIOptions();
        }

        [VerbOption(VERB_ADD, HelpText = "Adds a post")]
        public AddOptions AddVerb { get; set; }

        [VerbOption(VERB_IMPORT, HelpText = "Imports a post via BooruAPI")]
        public ImportOptions ImportVerb { get; set; }

        [VerbOption(VERB_DEL, HelpText = "Deletes a post")]
        public DelOptions DelVerb { get; set; }

        [VerbOption(VERB_EDIT, HelpText = "Edits a post")]
        public EditOptions EditVerb { get; set; }

        [VerbOption(VERB_SETIMG, HelpText = "Sets a posts image")]
        public SetImgOptions SetImgVerb { get; set; }

        [VerbOption(VERB_GUI, HelpText = "Import via GUI and URL in clipboard")]
        public GUIOptions GUIVerb { get; set; }

        [HelpVerbOption]
        public string GetUsage(string verb)
        {
            return HelpText.AutoBuild(this, verb);
        }
    }

    public class GUIOptions { }

    public class CommonOptionsId
    {
        [Option('i', "id", Required = true)]
        public uint ID { get; set; }
    }

    public class CommonOptionsAdd
    {
        [Option("info", Required = false)]
        public string Info { get; set; }

        [Option('r', "rating", DefaultValue = 3, Required = false)]
        public int Rating { get; set; }

        [Option("private", Required = false)]
        public bool Private { get; set; }

        [Option("force", Required = false)]
        public bool Force { get; set; }

        [Option('t', "tags", Required = false)]
        public string Tags { get; set; }
    }

    public class AddOptions : CommonOptionsAdd
    {
        [Option("image", Required = true)]
        public string ImagePath { get; set; }

        [Option('s', "source", Required = false)]
        public string Source { get; set; }
    }

    public class ImportOptions : CommonOptionsAdd
    {
        [Option('u', "url", Required = true)]
        public string URL { get; set; }

        [Option("all-tags", DefaultValue = false, Required = false, HelpText = "Add all tags, not only known tags")]
        public bool AllTags { get; set; }

        [Option("tags-no-delta", DefaultValue = false, Required = false, HelpText = "--tags defines the tags, not the tags delta")]
        public bool TagsNoDelta { get; set; }
    }

    public class DelOptions : CommonOptionsId { }

    public class EditOptions : CommonOptionsId
    {
        [Option("tags-no-delta", DefaultValue = false, Required = false, HelpText = "--tags defines the tags, not the tags delta")]
        public bool TagsNoDelta { get; set; }

        [Option('t', "tags", Required = false)]
        public string Tags { get; set; }

        [Option('s', "source", Required = false)]
        public string Source { get; set; }

        [Option("info", Required = false)]
        public string Info { get; set; }

        [Option('r', "rating", DefaultValue = -1, Required = false)]
        public int Rating { get; set; }

        [Option("private", Required = false)]
        public bool? Private { get; set; }
    }

    public class SetImgOptions
    {
        [Option('i', "id", Required = true)]
        public uint ID { get; set; }

        [Option("image", Required = true)]
        public string ImagePath { get; set; }
    }
}
