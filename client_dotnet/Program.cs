﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using teamalpha5441.Booru.Import;
using CommandLine;

namespace teamalpha5441.Booru.Client
{
    public static class Program
    {
        public static readonly Random Random = new Random();

        private static BooruClient _BooruClient;
        private static Importer _Importer;

        [STAThread]
        public static int Main(string[] args)
        {
            try { return MainStage2(args) ? 0 : 1; }
            catch (RemoteBooruException ex)
            {
                Console.Error.WriteLine("RemoteBooruException: " + ex.Message);
                // remote error -> no stack trace or inner exception needed
            }
            catch (Exception ex)
            {
                PrintException(ex);
                while (ex.InnerException != null)
                {
                    Console.WriteLine();
                    PrintException(ex.InnerException);
                    ex = ex.InnerException;
                }
            }
            return 1;
        }

        private static void PrintException(Exception ex, int depth = 0)
        {
            Console.WriteLine(ex.GetType().FullName + ": " + ex.Message);
            Console.WriteLine(ex.StackTrace);
        }

        public static bool MainStage2(string[] args)
        {
            // parse command line arguments
            string invokedVerb = null;
            object invokedVerbOptions = null;
            var options = new Options();
            var parseResult = Parser.Default.ParseArguments(args, options, (verb, subOptions) =>
            {
                invokedVerb = verb;
                invokedVerbOptions = subOptions;
            });
            if (!parseResult)
                return false;

            // load config and initialize clients and importer
            Config conf = Config.Load();
            // disable automatic version checking, check manually later
            _BooruClient = new BooruClient(conf.BooruURL, false);
            _BooruClient.Login(conf.BooruLogin.Username, conf.BooruLogin.Password);
            var logins = new Dictionary<string, APILogin>(conf.ImportLogins.Count);
            foreach (var confLogin in conf.ImportLogins)
                logins.Add(confLogin.Key, new APILogin()
                {
                    Username = confLogin.Value.Username,
                    Password = confLogin.Value.Password,
                });
            _Importer = new Importer(logins);

            // run gui if wanted
            if (invokedVerb == Options.VERB_GUI)
                try
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    _BooruClient.CheckVersion();
                    using (Form form = new GUI(_BooruClient, _Importer))
                        form.ShowDialog();

                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Booru Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            // run cli
            _BooruClient.CheckVersion();
            CLI cli = new CLI(_BooruClient, _Importer);
            return cli.Run(invokedVerb, invokedVerbOptions);
        }
    }
}
