using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace teamalpha5441.Booru.Client
{
    public class BooruClient : IDisposable
    {
        private const uint API_VERSION_MAJOR = 5;
        private const uint API_VERSION_MINOR_MIN = 1;
        private const int UPLOAD_TIMEOUT = 20 * 60 * 1000;

#pragma warning disable 0649

        private struct BooruSession
        {
            public string SID;
            public bool CanWrite;
        }

        private struct BooruAlias
        {
            public string Alias;
            public uint TID;
        }

#pragma warning restore 0649

        public struct BooruTag
        {
            public uint TID;
            public string Tag;
            public byte Type;
        }

        public struct BooruPost
        {
            public uint PID;
            public bool Private;
            public string Source, Info;
            public byte Rating;
            public BooruTag[] Tags;
        }

        public struct BooruInfo
        {
            public struct Version
            {
                public uint Major, Minor;
            }

            public Version APIVersion;
            public uint PostCount, ViewCount, TagCount;
        }

        private string _URL;
        private string _AuthHeader = null;
        private WebProxy _Proxy;

        public bool LoggedIn
        {
            get { return _AuthHeader != null; }
        }

        public BooruClient(string URL, bool CheckVersion = true, WebProxy Proxy = null)
        {
            _URL = URL;
            _Proxy = Proxy;
            if (CheckVersion)
                this.CheckVersion();
        }

        public void CheckVersion()
        {
            var info = GetInfo();
            string error = null;
            if (info.APIVersion.Major != API_VERSION_MAJOR)
                error = "API version major mismatch: server {0} ({0}.{1}) != client {2} ({2}.{3}+).";
            if (error == null && info.APIVersion.Minor < API_VERSION_MINOR_MIN)
                error = "servers API version minor too low: server {0}.{1} < client {2}.{3}+.";
            if (error != null)
            {
                error = string.Format(error, info.APIVersion.Major, info.APIVersion.Minor, API_VERSION_MAJOR, API_VERSION_MINOR_MIN);
                throw new RemoteBooruException(error);
            }
        }

        public void Login(string Username, string Password)
        {
            var str_data = "username=" + Uri.EscapeDataString(Username) + "&password=" + Uri.EscapeDataString(Password);
            var data = Encoding.UTF8.GetBytes(str_data);
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/session/login");
            request.Proxy = _Proxy;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            using (var str = request.GetRequestStream())
                str.Write(data, 0, data.Length);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                {
                    var session = new JsonSerializer().Deserialize<BooruSession>(jsonreader);
                    _AuthHeader = "Booru " + session.SID;
                }
            }
        }

        public void Logout()
        {
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/session/logout");
            request.Proxy = _Proxy;
            request.Method = "POST";
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
        }

        public void DeletePost(uint PID)
        {
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/posts/" + PID);
            request.Proxy = _Proxy;
            request.Method = "DELETE";
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
        }

        public void SetImage(uint PID, byte[] Image)
        {
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/posts/" + PID + "/image");
            request.Proxy = _Proxy;
            request.Method = "PUT";
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            request.ContentType = "application/octet-stream";
            request.ContentLength = Image.Length;
            request.Timeout = UPLOAD_TIMEOUT;
            using (var str = request.GetRequestStream())
                str.Write(Image, 0, Image.Length);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
        }

        public BooruPost GetPost(uint PID)
        {
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/posts/" + PID + "/meta");
            request.Proxy = _Proxy;
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                    return (BooruPost)(new JsonSerializer().Deserialize<BooruPost>(jsonreader));
            }
        }

        public BooruInfo GetInfo()
        {
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/info");
            request.Proxy = _Proxy;
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                    return (BooruInfo)(new JsonSerializer().Deserialize<BooruInfo>(jsonreader));
            }
        }

        public uint UploadPost(byte[] Image, bool Private, string Source, string Info, byte Rating, string[] Tags, bool Force)
        {
            var post = new BooruPost()
            {
                Private = Private,
                Source = Source,
                Info = Info,
                Rating = Rating,
                Tags = new BooruTag[Tags.Length]
            };
            for (int i = 0; i < Tags.Length; i++)
                post.Tags[i] = new BooruTag() { Tag = Tags[i].ToLower().Trim() };
            byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(post));
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/posts");
            request.Proxy = _Proxy;
            request.Method = "POST";
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            request.ContentType = "application/json";
            request.ContentLength = postData.Length;
            using (var str = request.GetRequestStream())
                str.Write(postData, 0, postData.Length);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                    post = (BooruPost)(new JsonSerializer().Deserialize<BooruPost>(jsonreader));
            }
            SetImage(post.PID, Image);
            return post.PID;
        }

        public void UpdatePost(BooruPost Post, string[] Tags = null)
        {
            if (Tags != null)
            {
                Post.Tags = new BooruTag[Tags.Length];
                for (int i = 0; i < Tags.Length; i++)
                    Post.Tags[i] = new BooruTag() { Tag = Tags[i].ToLower().Trim() };
            }
            byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(Post));
            var request = (HttpWebRequest)WebRequest.Create(_URL + "/posts/" + Post.PID + "/meta");
            request.Proxy = _Proxy;
            request.Method = "PUT";
            if (_AuthHeader != null)
                request.Headers.Add(HttpRequestHeader.Authorization, _AuthHeader);
            request.ContentType = "application/json";
            request.ContentLength = postData.Length;
            using (var str = request.GetRequestStream())
                str.Write(postData, 0, postData.Length);
            using (var response = (HttpWebResponse)request.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
        }

        public string[] GetAllTagsAndAliases()
        {
            BooruTag[] bTags;
            var request1 = (HttpWebRequest)WebRequest.Create(_URL + "/extra/tags");
            request1.Proxy = _Proxy;
            using (var response = (HttpWebResponse)request1.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                    bTags = (BooruTag[])(new JsonSerializer().Deserialize<BooruTag[]>(jsonreader));
            }

            BooruAlias[] bAliases;
            var request2 = (HttpWebRequest)WebRequest.Create(_URL + "/extra/aliases");
            request2.Proxy = _Proxy;
            using (var response = (HttpWebResponse)request2.GetResponse())
            using (var str = response.GetResponseStream())
            using (var reader = new StreamReader(str))
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception("HTTP " + response.StatusCode + " - " + reader.ReadToEnd());
                using (var jsonreader = new JsonTextReader(reader))
                    bAliases = (BooruAlias[])(new JsonSerializer().Deserialize<BooruAlias[]>(jsonreader));
            }

            string[] all = new string[bTags.Length + bAliases.Length];
            for (int i = 0; i < bTags.Length; i++)
                all[i] = bTags[i].Tag;
            for (int i = 0; i < bAliases.Length; i++)
                all[bTags.Length + i] = bAliases[i].Alias;
            return all;
        }

        public void Dispose()
        {
            try { Logout(); }
            catch { }
        }
    }
}
