var booruApp = angular.module('booruApp');

booruApp.controller('mainController', function($scope, $state) {
	$scope.query = "";
	$scope.query_wip = "";
	$scope.search = function(event) {
		event.preventDefault();
		$scope.query = $scope.query_wip;
		$state.go("search", { query: $scope.query });
	};
});

booruApp.controller('homeController', function($scope, $http) {
	$scope.$parent.query = "";
	$scope.$parent.query_wip = "";
	$http({
		method: 'GET',
		url: '/api/info'
	}).then(function(response) {
		$scope.digits = response.data.PostCount.toString().split('');
	}, function(response) {
		$scope.digits = [ '0' ];
		console.error(response);
	});
});

booruApp.controller('postsController', function($scope, $http, $stateParams) {
	if ("query" in $stateParams) {
		$scope.$parent.query = $stateParams.query;
		$scope.$parent.query_wip = $stateParams.query;
	} else {
		$scope.$parent.query = "";
		$scope.$parent.query_wip = "";
	}
	$scope.posts = [];
	$scope.busy = false;
	$scope.loadMore = function() {
		$scope.busy = true;
		var url = '/api/posts'
		if ($scope.$parent.query.length > 0) {
			url = '/api/search/' + encodeURIComponent($scope.$parent.query);
		}
		$http({
			method: 'GET',
			url: url,
			params: {
				offset: $scope.posts.length
			}
		}).then(function(response) {
			for (var i = 0; i < response.data.Posts.length; i++) {
				$scope.posts.push(response.data.Posts[i]);
			}
			if (response.data.Posts.length > 0)
				$scope.busy = false;
			$scope.footer = "showing " + $scope.posts.length + " of " + response.data.Total + " posts";
		}, function(response) {
			$scope.footer = "[" + response.status + " " + response.statusText + "] " + response.data;
			console.error(response);
		});
	};
	$scope.loadMore();
});

booruApp.controller('postController', function($scope, $http, $stateParams) {
	$http({
		method: 'GET',
		url: '/api/posts/' + $stateParams.pid + '/meta'
	}).then(function(response) {
		$scope.post = response.data;
		$scope.post.ImageMimeCategory = $scope.post.ImageMime.split('/')[0];
		$scope.post.ImageURL = '/api/posts/' + $scope.post.PID + '/image';
		$scope.post.Tags.sort(function(a, b) {
			if (a.Type == b.Type)
				return a.Tag.localeCompare(b.Tag)
			return b.Type - a.Type;
		});
	}, function(response) {
		console.error(response);
	});
});
