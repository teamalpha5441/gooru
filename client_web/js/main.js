var booruApp = angular.module('booruApp', ['ui.router', 'infinite-scroll']);

booruApp.config(function($locationProvider, $stateProvider, $urlRouterProvider) {
	$locationProvider.html5Mode(true);
	$urlRouterProvider.otherwise('/home')
	$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: '/pages/home.htm',
			controller: 'homeController'
		})
		// all posts
		.state('posts', {
			url: '/posts',
			templateUrl: '/pages/posts.htm',
			controller: 'postsController'
		})
		// search posts
		.state('search', {
			url: '/search/{query}',
			templateUrl: '/pages/posts.htm',
			controller: 'postsController'
		})
		// single post
		.state('post', {
			url: '/posts/{pid:[0-9]+}',
			templateUrl: '/pages/post.htm',
			controller: 'postController'
		});
});
