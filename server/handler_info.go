package main

import (
	"net/http"
)

func handlerGetInfo(w http.ResponseWriter, r *http.Request) {
	post_count, view_count, tag_count, err := Backend.GetCountStats()
	if err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't get counters")
		return
	}
	api_version := struct {
		Major uint
		Minor uint
	}{
		API_VERSION_MAJOR,
		API_VERSION_MINOR,
	}
	data := struct {
		APIVersion interface{}
		PostCount uint
		ViewCount uint
		TagCount  uint
	}{
		api_version,
		post_count,
		view_count,
		tag_count,
	}
	w.Header().Set("Cache-Control", "max-age=60")
	handlerReturnJson(w, data)
}
