package main

import (
	"net/http"
)

func handlerGetExtraTags(w http.ResponseWriter, r *http.Request) {
	tags, err := Backend.GetTags()
	if err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't get tags")
		return
	}
	handlerReturnJson(w, tags)
}

func handlerGetExtraAliases(w http.ResponseWriter, r *http.Request) {
	aliases, err := Backend.GetAliases()
	if err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't get aliases")
		return
	}
	handlerReturnJson(w, aliases)
}
