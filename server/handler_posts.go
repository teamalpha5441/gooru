package main

import (
	"bitbucket.org/teamalpha5441/Gooru/server/backend"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

func getPostFromPostsHandler(w http.ResponseWriter, r *http.Request, checkWriteAccess bool) (backend.BooruPost, bool) {
	pid, _ := strconv.ParseUint(mux.Vars(r)["pid"], 10, 32)
	post, err := Backend.GetPost(uint(pid), true)
	if err != nil {
		handlerError(w, http.StatusNotFound, err.Error(), "couldn't get post")
		return post, false
	}
	sess, ok := handlerGetSession(w, r, checkWriteAccess)
	if !ok {
		return post, false
	}
	if checkWriteAccess && !sess.IsAdmin && !sess.CanWrite {
		errmsg := "no permission to write post"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return post, false
	}
	if !post.CanUserViewPost(sess) {
		errmsg := "no permission to view post"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return post, false
	}
	return post, true
}

func handlerGetPostsMeta(w http.ResponseWriter, r *http.Request) {
	if post, ok := getPostFromPostsHandler(w, r, false); ok {
		handlerReturnJson(w, post)
	}
}

func handlerGetPostsThumb(w http.ResponseWriter, r *http.Request) {
	if post, ok := getPostFromPostsHandler(w, r, false); ok {
		path := Backend.GetThumbPath(post.PID, post.ThumbMime, true)
		w.Header().Set("Content-Type", post.ThumbMime)
		http.ServeFile(w, r, path)
	}
}

func handlerGetPostsImage(w http.ResponseWriter, r *http.Request) {
	if post, ok := getPostFromPostsHandler(w, r, false); ok {
		path := Backend.GetImagePath(post.PID, post.ImageMime, true)
		w.Header().Set("Content-Type", post.ImageMime)
		http.ServeFile(w, r, path)
	}
}

func handlerPutPostsImage(w http.ResponseWriter, r *http.Request) {
	post, ok := getPostFromPostsHandler(w, r, true)
	if !ok {
		return
	}
	// read whole image into memory
	imgData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't read request body")
		return
	}
	// save image to file
	if err := Backend.SetImage(post.PID, imgData); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't save image")
		return
	}
}

func handlerPutPostsMeta(w http.ResponseWriter, r *http.Request) {
	sess, ok := handlerGetSession(w, r, true)
	if !ok {
		return
	}
	if !(sess.IsAdmin || sess.CanWrite) {
		errmsg := "no permission to create post"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return
	}
	jsonData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't read request body")
		return
	}
	var bp backend.BooruPost
	if err = json.Unmarshal(jsonData, &bp); err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't unmarshal json")
		return
	}
	url_pid, _ := strconv.ParseUint(mux.Vars(r)["pid"], 10, 32)
	if bp.PID != uint(url_pid) {
		errmsg := "url pid != json pid"
		handlerError(w, http.StatusBadRequest, errmsg, errmsg)
		return
	}
	if err = Backend.UpdatePostMeta(bp); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't update post")
		return
	}
	tags := make([]string, len(bp.Tags))
	for i, bt := range bp.Tags {
		tags[i] = bt.Tag
	}
	if err = Backend.SetPostTags(bp.PID, tags); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't set post-tags")
	}
}

func handlerPostPosts(w http.ResponseWriter, r *http.Request) {
	sess, ok := handlerGetSession(w, r, true)
	if !ok {
		return
	}
	if !(sess.IsAdmin || sess.CanWrite) {
		errmsg := "no permission to create post"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return
	}
	jsonData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't read request body")
		return
	}
	var bp backend.BooruPost
	if err = json.Unmarshal(jsonData, &bp); err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't unmarshal json")
		return
	}
	bp.PID, err = Backend.CreatePost(sess.UID)
	if err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't create post")
		return
	}
	if err = Backend.UpdatePostMeta(bp); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't update created post")
		return
	}
	tags := make([]string, len(bp.Tags))
	for i, bt := range bp.Tags {
		tags[i] = bt.Tag
	}
	if err = Backend.SetPostTags(bp.PID, tags); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't set post-tags")
		return
	}
	data := struct{ PID uint }{bp.PID}
	handlerReturnJson(w, data)
}

func handlerDeletePosts(w http.ResponseWriter, r *http.Request) {
	if post, ok := getPostFromPostsHandler(w, r, true); ok {
		if err := Backend.DeletePost(post); err != nil {
			handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't delete post")
		}
	}
}
