package main

import (
	"bitbucket.org/teamalpha5441/Gooru/server/backend"
	"bitbucket.org/teamalpha5441/Gooru/server/imaging"
	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
	"net/http"
	"os"
)

const (
	AVATAR_SIZE = 128
)

func getUidFromUsersHandler(w http.ResponseWriter, r *http.Request) (uuid.UUID, bool) {
	uuid, err := uuid.FromString(mux.Vars(r)["uid"])
	if err != nil {
		errmsg := "couldn't parse uuid"
		handlerError(w, http.StatusBadRequest, errmsg, errmsg)
		return uuid, false
	}
	return uuid, true
}

func handlerGetUsersAvatar(w http.ResponseWriter, r *http.Request) {
	uid, ok := getUidFromUsersHandler(w, r)
	if !ok {
		return
	}
	path := Backend.GetAvatarPath(uid, true)
	w.Header().Set("Content-Type", backend.AVATAR_MIME)
	http.ServeFile(w, r, path)
}

func handlerPutUsersAvatar(w http.ResponseWriter, r *http.Request) {
	uid, ok := getUidFromUsersHandler(w, r)
	if !ok {
		return
	}
	sess, ok := handlerGetSession(w, r, true)
	if !ok {
		return
	}
	if !(sess.IsAdmin || uuid.Equal(uid, sess.UID)) {
		errmsg := "no permission to set avatar"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return
	}
	img, err := imaging.Read(r.Body)
	if err != nil {
		handlerError(w, http.StatusBadRequest, err.Error(), "couldn't read image")
		return
	}
	avatar := imaging.CreateThumbnail(img, AVATAR_SIZE)
	path := Backend.GetAvatarPath(uid, false)
	if err = imaging.SaveJPG(avatar, path); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't save image")
	}
}

func handlerDeleteUsersAvatar(w http.ResponseWriter, r *http.Request) {
	uid, ok := getUidFromUsersHandler(w, r)
	if !ok {
		return
	}
	sess, ok := handlerGetSession(w, r, true)
	if !ok {
		return
	}
	if !(sess.IsAdmin || uuid.Equal(uid, sess.UID)) {
		errmsg := "no permission to delete avatar"
		handlerError(w, http.StatusForbidden, errmsg, errmsg)
		return
	}
	if err := os.RemoveAll(Backend.GetAvatarPath(uid, false)); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "couldn't delete avatar")
	}
}
