package main

import (
	"net/http"
)

func handlerPostSessionLogin(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")
	sess, err := Backend.LoginUser(username, password)
	if err != nil {
		handlerError(w, http.StatusUnauthorized, err.Error(), "authentication failed")
		return
	}
	handlerReturnJson(w, sess)
}

func handlerPostSessionLogout(w http.ResponseWriter, r *http.Request) {
	sess, ok := handlerGetSession(w, r, true)
	if !ok {
		return
	}
	if err := Backend.LogoutUser(sess.SID); err != nil {
		handlerError(w, http.StatusInternalServerError, err.Error(), "logout failed")
		return
	}
}
