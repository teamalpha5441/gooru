GET /info
=========

Session
-------

not needed

GET Parameters
--------------

no parameters

Response
--------

```javascript
{
	"API": 1,
	"PostCount": 1234,
	"ViewCount": 1234,
	"TagCount": 1234
}
```

GET /posts/search
================

Session
-------

optional (private posts)

GET Parameters
--------------

Type | Name | Description | Optional
--- | --- | --- | ---
string | query | search query | optional
uint | offset | post offset | optional
uint | count | post count (limited) | optional

Response
--------

```javascript
{
	"Count": 40,
	"Total": 256000,
	"Posts":
	[
		{
			"PID": 1234,
			"Owner": true,
			"Private": false,
			"Rating": 1,
			"Width": 1920,
			"Height": 1080,
			"MimeType": "image/png"
		}
	]
}
```

GET /posts/image
===============

Session
-------

optional (private posts)

GET Parameters
--------------

Type | Name | Description
--- | --- | ---
uint | pid | post id
byte | size | 1 = thumbnail, 2 = reserved, 3 = full size image

Response
--------

image/?

GET /posts/meta
==============

Session
-------

optional (private posts)

GET Parameters
--------------

Type | Name | Description
--- | --- | ---
uint | pid | post id

Response
--------

```javascript
{
	"PID": 1234,
	"User": "<36 chars uuid>",
	"Private": false,
	"Source": "http://example.com/",
	"Info": "example post",
	"Rating": 1,
	"Width": 1920,
	"Height": 1080,
	"Created": 1483150702,
	"MimeType": "image/png",
	"Tags":
	[
		{
			"Type": 2,
			"Tag": "hatsune_miku"
		}
	]
```

POST /users/login
================

Session
-------

not allowed

POST Parameters
---------------

Type | Name
--- | ---
string | username
string | password

Response
--------

```javascript
{
	"UID": "<36 chars uuid>",
	"SID": "<32 chars session identifier>",
	"Perms":
	[
		"booru.admin",
		"booru.upload"
	]
}
```

GET /users/logout
================

Session
-------

needed

GET Parameters
--------------

no parameters

Response
--------

```javascript
{}
```

GET /users/avatar
================

Session
-------

not needed

GET Parameters
--------------

Type | Name | Description
--- | --- | ---
uuid | uid | user id

Response
--------

image/?
