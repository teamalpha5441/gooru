package backend

import (
	kuroauth "bitbucket.org/teamalpha5441/KuroAuth/client"
	"errors"
	"github.com/satori/go.uuid"
)

const (
	PERM_PREFIX = "booru"
	PERM_WRITE  = "booru.write"
	PERM_ADMIN  = "booru.admin"
)

var (
	USERCACHE     = make(map[uuid.UUID]string)
	GUEST_SESSION = Session{uuid.Nil, "guest", "", false, false}
)

type Session struct {
	UID               uuid.UUID
	Username, SID     string
	CanWrite, IsAdmin bool
}

func (s Session) IsLoggedIn() bool {
	return !uuid.Equal(s.UID, uuid.Nil)
}

func parsePerms(perms []string) (canWrite bool, isAdmin bool) {
	canWrite, isAdmin = false, false
	for _, p := range perms {
		if p == PERM_WRITE {
			canWrite = true
		} else if p == PERM_ADMIN {
			canWrite = true
			isAdmin = true
		}
	}
	return
}

//TODO make immune against timing attacks
func (b Backend) LoginUser(username, password string) (Session, error) {
	kac, err := kuroauth.Connect()
	sess := Session{}
	if err != nil {
		return sess, err
	}
	defer kac.Close()
	sess.UID, err = kac.AuthenticateUser(username, password)
	if err != nil {
		return sess, err
	}
	// save username in usercache and sess struct
	USERCACHE[sess.UID], sess.Username = username, username
	sess.SID, err = kac.CreateSession(sess.UID)
	if err != nil {
		return sess, err
	}
	perms, err := kac.GetPermissions(PERM_PREFIX, sess.UID)
	if err == nil {
		sess.CanWrite, sess.IsAdmin = parsePerms(perms)
	}
	return sess, err
}

func (b Backend) GetSession(sid string) (Session, error) {
	kac, err := kuroauth.Connect()
	sess := Session{SID: sid}
	if err != nil {
		return sess, err
	}
	defer kac.Close()
	sess.UID, err = kac.GetSession(sid)
	if err != nil {
		return sess, err
	}
	if username, ok := USERCACHE[sess.UID]; !ok {
		sess.Username, err = retrieveUsername(kac, sess.UID)
		if err != nil {
			return sess, err
		}
		USERCACHE[sess.UID] = sess.Username
	} else {
		sess.Username = username
	}
	perms, err := kac.GetPermissions(PERM_PREFIX, sess.UID)
	if err == nil {
		sess.CanWrite, sess.IsAdmin = parsePerms(perms)
	}
	return sess, err
}

func (b Backend) LogoutUser(sid string) error {
	kac, err := kuroauth.Connect()
	if err != nil {
		return err
	}
	defer kac.Close()
	return kac.DestroySession(sid)
}

func (b Backend) GetUsername(uid uuid.UUID) (string, error) {
	if username, ok := USERCACHE[uid]; ok {
		return username, nil
	}
	kac, err := kuroauth.Connect()
	if err != nil {
		return "", err
	}
	defer kac.Close()
	username, err := retrieveUsername(kac, uid)
	if err == nil {
		USERCACHE[uid] = username
	}
	return username, err
}

func retrieveUsername(kac *kuroauth.KuroAuthClient, uid uuid.UUID) (string, error) {
	profile, err := kac.GetProfile(uid)
	if err != nil {
		return "", err
	}
	username, ok := profile["Username"]
	if !ok {
		return username, errors.New("username not found in profile")
	}
	return username, nil
}
