package backend

import (
	"errors"
	"fmt"
	"sort"
	"strings"
)

const (
	MAX_SEARCH_TERMS = 8
	MAX_POST_COUNT   = 64
)

var (
	SPECIAL_TERM_VARIABLES = map[string]string{
		"i": "id",
		//"u": "user", // handled differently (owner is user)
		//"f": "favorite", // handled differently (favorite of user)
		"a": "width / height", // handled differently (aspect ratio)
		"w": "width",
		"h": "height",
		"r": "rating",
		"p": "private",
	}
	SPECIAL_TERM_OPERATORS = []string{"<", ">", "="}
)

type SearchQuery struct {
	Query string
	Args  []interface{}
}

func appendUnique(a []string, s string) []string {
	for _, e := range a {
		if e == s {
			return a
		}
	}
	return append(a, s)
}

func CompileSearch(term string, columns string, offset, count uint, sess Session) (SearchQuery, error) {
	result := SearchQuery{}
	if count > MAX_POST_COUNT || count < 1 {
		count = MAX_POST_COUNT
	}

	// split query in parts/terms (ignore extra whitespaces)
	parts := []string{}
	for _, part := range strings.Split(term, " ") {
		part = strings.TrimSpace(part)
		if len(part) > 0 {
			parts = appendUnique(parts, part)
		}
	}

	// check if there are way too many search terms (only if not logged in)
	if !sess.IsLoggedIn() && (len(parts) > 2*MAX_SEARCH_TERMS) {
		return result, errors.New(fmt.Sprintf("way too many search terms (%d >> %d)", len(parts), MAX_SEARCH_TERMS))
	}

	// group normal and negated terms in two separate arrays
	pos_parts := make([]string, 0, len(parts))
	neg_parts := make([]string, 0, len(parts))
	for _, part := range parts {
		if part[0:1] == "!" || part[0:1] == "_" {
			if len(part) < 2 {
				return result, errors.New("tried to invert empty term")
			}
			neg_parts = append(neg_parts, part[1:])
		} else {
			pos_parts = append(pos_parts, part)
		}
	}

	// check if there are too many search terms (only if not logged in)
	if !sess.IsLoggedIn() && (len(pos_parts)+len(neg_parts) > MAX_SEARCH_TERMS) {
		return result, errors.New(fmt.Sprintf("too many search terms (%d > %d)", len(pos_parts)+len(neg_parts), MAX_SEARCH_TERMS))
	}

	// check for tag and tag negation in terms
	for _, p := range pos_parts {
		for _, n := range neg_parts {
			if p == n {
				return result, errors.New("tag and tag negation in query (" + p + ")")
			}
		}
	}

	// sort both term arrays
	sort.Strings(pos_parts)
	sort.Strings(neg_parts)

	//TODO hash both term arrays and check cache
	//string hash = ...

	// generate subqueries from search terms
	subqueries := make([]string, 0, 1+len(pos_parts)+len(neg_parts))
	result.Args = make([]interface{}, 0, cap(subqueries))
	for _, term := range pos_parts {
		subquery, param, err := parseTerm(true, term)
		if err != nil {
			return result, err
		}
		subqueries = append(subqueries, subquery)
		result.Args = append(result.Args, param)
	}
	for _, term := range neg_parts {
		subquery, param, err := parseTerm(false, term)
		if err != nil {
			return result, err
		}
		subqueries = append(subqueries, subquery)
		result.Args = append(result.Args, param)
	}
	if !sess.IsLoggedIn() {
		subqueries = append(subqueries, "private = 0")
	} else if !sess.IsAdmin {
		subqueries = append(subqueries, "(private = 0 OR user = ?)")
		result.Args = append(result.Args, sess.UID)
	}

	// generate main sql query
	result.Query = "SELECT SQL_CALC_FOUND_ROWS " + columns + " FROM posts WHERE 1"
	for _, sq := range subqueries {
		result.Query += " AND " + sq
	}
	result.Query += " ORDER BY created DESC LIMIT ? OFFSET ?"
	result.Args = append(result.Args, count, offset)

	//TODO save compiled query in cache

	return result, nil
}

func parseTerm(sign bool, term string) (string, string, error) {
	if term[0:1] != ":" {
		in := "id NOT IN"
		if sign {
			in = "id IN"
		}
		return in + " (SELECT post_id FROM post_tags WHERE tag_id = (SELECT id FROM tags WHERE tag = ?))", term, nil
	} else {
		if len(term) < 4 {
			return term, "", errors.New("special term syntax error")
		}
		variable, operator, value, ok := term[1:2], term[2:3], term[3:], true
		if variable, ok = SPECIAL_TERM_VARIABLES[variable]; !ok {
			return term, "", errors.New("special term unknown variable")
		}
		if operator != "<" && operator != ">" && operator != "=" {
			return term, "", errors.New("special term unknown operator")
		}
		/*
			if sterm.Variable == "user" || sterm.Variable == "favorite" {
				if operator != "=" {
					return sterm, errors.New("wrong operator for this variable")
				}

								if (session_loggedin())
								{
									if ($s_var == "u")
										return new SearchTerm("user_id = ?", "i", session_user_id());
									else return new SearchTerm("id IN (SELECT post_id FROM favorites WHERE user_id = ?)", "i", session_user_id());
								}
								else return "You must be logged in to use the username placeholder";
				if sterm.Variable == "u" {
					sterm.SQL = "user_id = (SELECT id FROM users WHERE username = ?)"
				} else if sterm.Variable == "f" {
					//TODO implement "f" support
					sterm.SQL = "id IN (SELECT post_id FROM favorites WHERE user_id = (SELECT id FROM users WHERE username = ?))"
					return sterm, errors.New("not yet implemented") // remove this line after implementation
				}
			}
		*/
		return variable + " " + operator + " ?", value, nil
	}
}

/*
		$search_result = new SearchResult();
		$search_result->count = $result->num_rows;
		while ($row = $result->fetch_assoc())
		{
			$id = $row["id"];
			$search_result->ids[] = $id;
			$search_result->info[$id] = $row;
			$search_result->info[$id]["favorite"] = 0;
		}
		$search_result->count_all = $db->x_found_rows();

		$esoa_result = $db->booru_posts_have_tag($search_result->ids, "esoa");
		$animated_result = $db->booru_posts_have_tag($search_result->ids, "animated");
		foreach ($search_result->ids as $post_id)
		{
			$search_result->info[$post_id]["esoa"] = $esoa_result[$post_id] ? 1 : 0;
			$search_result->info[$post_id]["animated"] = $animated_result[$post_id] ? 1 : 0;
		}

		if (session_loggedin())
		{
			$fav_result = $db->booru_posts_are_favorites($search_result->ids, session_user_id());
			foreach ($search_result->ids as $post_id)
				if ($fav_result[$post_id])
					$search_result->info[$post_id]["favorite"] = 1;
		}

		return $search_result;
	}
	catch (Exception $ex) { return $ex->getMessage(); }
*/
