package backend

import (
	"database/sql"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/satori/go.uuid"
	"strconv"
	"time"
)

const (
	MAX_RATING   = 4
	MAX_TAG_TYPE = byte(7)

	BOORUPOST_COLUMNS  = "id, user, private, source, info, rating, width, height, created, thumb_mime, image_mime, hash, phash"
	SEARCHPOST_COLUMNS = "id, user, private, rating, width, height, image_mime"
)

type SearchPost struct {
	PID       uint
	Owner     bool
	Private   bool
	Rating    byte
	Width     uint
	Height    uint
	ImageMime string
}

type Database struct {
	Database *sql.DB
}

func OpenDatabase(DSN string) (Database, error) {
	db, err := sql.Open("mysql", DSN)
	return Database{db}, err
}

func (bdb Database) Close() {
	bdb.Database.Close()
}

func (bdb Database) Ping() error {
	return bdb.Database.Ping()
}

func checkExecResult(res sql.Result, min_count, max_count int64) error {
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if min_count >= 0 && count < min_count {
		return errors.New("affected row count < " + strconv.FormatInt(min_count, 10))
	}
	if max_count >= 0 && count > max_count {
		return errors.New("affected row count > " + strconv.FormatInt(max_count, 10))
	}
	return nil
}

/*
func (bdb Database) CanUserViewPost(sess Session, pid uint) (bool, error) {
	if sess.HasPermission(PERM_ADMIN) {
		return true, nil
	}
	var str_user string
	var private bool
	if err = bdb.Database.QueryRow("SELECT user, private FROM posts WHERE id = ?", pid).Scan(&str_user, &private); err != nil {
		return false, err
	}
	user, _ := uuid.FromString(str_user)
	return uuid.Equal(user, sess.UID), nil
}
*/

// returns post count, total views
func (bdb Database) GetCountStats() (count, views, tags uint, err error) {
	if err = bdb.Database.QueryRow("SELECT COUNT(*) AS count, SUM(viewcount) AS views FROM posts").Scan(&count, &views); err != nil {
		return
	}
	err = bdb.Database.QueryRow("SELECT COUNT(*) FROM tags").Scan(&tags)
	return
}

func (bdb Database) GetPost(pid uint) (p BooruPost, err error) {
	err = bdb.Database.QueryRow("SELECT "+BOORUPOST_COLUMNS+" FROM posts WHERE id = ?", pid).Scan(&p.PID, &p.User, &p.Private, &p.Source, &p.Info, &p.Rating, &p.Width, &p.Height, &p.Created, &p.ThumbMime, &p.ImageMime, &p.Hash, &p.PHash)
	return
}

// inserts new post into database, only owner and creation date set
func (bdb Database) InsertPost(uid uuid.UUID) (uint, error) {
	res, err := bdb.Database.Exec("INSERT INTO posts (user, created) VALUES (?, ?)", uid.String(), time.Now().Unix())
	if err != nil {
		return 0, err
	}
	if err = checkExecResult(res, 1, 1); err != nil {
		return 0, err
	}
	pid, err := res.LastInsertId()
	return uint(pid), err
}

func (bdb Database) InsertTag(tag string) (BooruTag, error) {
	res, err := bdb.Database.Exec("INSERT INTO tags (tag, type) VALUES (?, ?)", tag, MAX_TAG_TYPE)
	if err != nil {
		return BooruTag{}, err
	}
	if err = checkExecResult(res, 1, 1); err != nil {
		return BooruTag{}, err
	}
	tid, err := res.LastInsertId()
	return BooruTag{uint(tid), tag, MAX_TAG_TYPE}, err
}

func (bdb Database) GetTags() ([]BooruTag, error) {
	tags := make([]BooruTag, 0, 8000)
	rows, err := bdb.Database.Query("SELECT id, tag, type FROM tags")
	if err != nil {
		return tags, err
	}
	defer rows.Close()
	for rows.Next() {
		tag := BooruTag{}
		if err = rows.Scan(&tag.TID, &tag.Tag, &tag.Type); err != nil {
			return tags, err
		}
		tags = append(tags, tag)
	}
	return tags, nil
}

func (bdb Database) GetAliases() ([]BooruAlias, error) {
	aliases := make([]BooruAlias, 0, 800)
	rows, err := bdb.Database.Query("SELECT alias, tag_id FROM aliases")
	if err != nil {
		return aliases, err
	}
	defer rows.Close()
	for rows.Next() {
		alias := BooruAlias{}
		if err = rows.Scan(&alias.Alias, &alias.TID); err != nil {
			return aliases, err
		}
		aliases = append(aliases, alias)
	}
	return aliases, nil
}

// updates the post meta information (private, source, info, rating)
func (bdb Database) UpdatePostMeta(bp BooruPost) error {
	priv := 0
	if bp.Private {
		priv = 1
	}
	if bp.Rating < 1 || bp.Rating > MAX_RATING {
		return errors.New("post rating out of range")
	}
	res, err := bdb.Database.Exec("UPDATE posts SET private = ?, source = ?, info = ?, rating = ? WHERE id = ?", priv, bp.Source, bp.Info, bp.Rating, bp.PID)
	if err != nil {
		return err
	}
	return checkExecResult(res, 0, 1)
}

// update the image meta information (width, height, thumb_mime, image_mime, hash, phash)
func (bdb Database) UpdateImageMeta(bp BooruPost) error {
	res, err := bdb.Database.Exec("UPDATE posts SET width = ?, height = ?, thumb_mime = ?, image_mime = ?, hash = ?, phash = ? WHERE id = ?", bp.Width, bp.Height, bp.ThumbMime, bp.ImageMime, bp.Hash, bp.PHash, bp.PID)
	if err != nil {
		return err
	}
	return checkExecResult(res, 0, 1)
}

func (bdb Database) DeletePost(pid uint) error {
	res, err := bdb.Database.Exec("DELETE FROM posts WHERE id = ?", pid)
	if err != nil {
		return err
	}
	return checkExecResult(res, 0, 1)
}

func (bdb Database) DeletePostTags(pid uint) error {
	res, err := bdb.Database.Exec("DELETE FROM post_tags WHERE post_id = ?", pid)
	if err != nil {
		return err
	}
	return checkExecResult(res, 0, -1)
}

func (bdb Database) InsertPostTags(pid uint, tids []uint) error {
	stmt, err := bdb.Database.Prepare("INSERT INTO post_tags (post_id, tag_id) VALUES (?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, tid := range tids {
		res, err := stmt.Exec(pid, tid)
		if err != nil {
			return err
		}
		if err = checkExecResult(res, 1, 1); err != nil {
			return err
		}
	}
	return nil
}

func (bdb Database) ResolveAlias(alias string) (uint, bool, error) {
	rows, err := bdb.Database.Query("SELECT tag_id FROM aliases WHERE alias = ?", alias)
	if err != nil {
		return 0, false, err
	}
	defer rows.Close()
	if rows.Next() {
		var tid uint
		err = rows.Scan(&tid)
		return tid, true, err
	}
	return 0, false, nil
}

func (bdb Database) GetTag(tid uint) (tag BooruTag, err error) {
	err = bdb.Database.QueryRow("SELECT tag, type FROM tags WHERE id = ?", tid).Scan(&tag.Tag, &tag.Type)
	tag.TID = tid
	return
}

func (bdb Database) ResolveTag(tag string) (BooruTag, bool, error) {
	var btag BooruTag
	rows, err := bdb.Database.Query("SELECT id, tag, type FROM tags WHERE tag = ?", tag)
	if err != nil {
		return btag, false, err
	}
	defer rows.Close()
	if rows.Next() {
		err = rows.Scan(&btag.TID, &btag.Tag, &btag.Type)
		return btag, true, err
	}
	return btag, false, nil
}

func (bdb Database) GetPostTags(pid uint) ([]BooruTag, error) {
	tags := make([]BooruTag, 0, 64)
	rows, err := bdb.Database.Query("SELECT id, tag, type FROM tags WHERE id IN (SELECT tag_id FROM post_tags WHERE post_id = ?)", pid)
	if err != nil {
		return tags, err
	}
	defer rows.Close()
	var tid uint
	var ttype byte
	var tag string
	for rows.Next() {
		if err = rows.Scan(&tid, &tag, &ttype); err != nil {
			return tags, err
		}
		tags = append(tags, BooruTag{tid, tag, ttype})
	}
	return tags, nil
}

func (bdb Database) SearchPosts(term string, offset, count uint, sess Session) ([]SearchPost, uint, bool, error) {
	posts := make([]SearchPost, 0, MAX_POST_COUNT)
	search, err := CompileSearch(term, SEARCHPOST_COLUMNS, offset, count, sess)
	if err != nil {
		return posts, 0, false, err
	}
	// transaction is needed for SQL_CALC_FOUND_ROWS and FOUND_ROWS()
	tx, err := bdb.Database.Begin()
	if err != nil {
		return posts, 0, true, err
	}
	defer tx.Rollback()
	rows, err := tx.Query(search.Query, search.Args...)
	if err != nil {
		return posts, 0, true, err
	}
	defer rows.Close()
	var p SearchPost
	var user uuid.UUID
	for rows.Next() {
		err = rows.Scan(&p.PID, &user, &p.Private, &p.Rating, &p.Width, &p.Height, &p.ImageMime)
		if err != nil {
			return posts, uint(len(posts)), true, err
		}
		p.Owner = uuid.Equal(user, sess.UID)
		posts = append(posts, p)
	}
	var pcount uint
	err = tx.QueryRow("SELECT FOUND_ROWS()").Scan(&pcount)
	return posts, pcount, true, err
}
