package backend

import (
	"bitbucket.org/teamalpha5441/Gooru/server/imaging"
	"bitbucket.org/teamalpha5441/Gooru/server/mediainfo"
	"github.com/satori/go.uuid"
	"os"
)

type BooruTag struct {
	TID  uint
	Tag  string
	Type byte
}

type BooruAlias struct {
	Alias string
	TID uint
}

type BooruPost struct {
	PID     uint
	User    uuid.UUID
	Private bool
	Source  string
	Info    string
	Rating  byte
	Width   uint
	Height  uint
	Created uint64
	// ViewCount uint
	ThumbMime string
	ImageMime string
	Hash      string
	PHash     uint64
	Tags      []BooruTag
}

func (b BooruPost) CanUserViewPost(sess Session) bool {
	return !b.Private || sess.IsAdmin || uuid.Equal(b.User, sess.UID)
}

type Backend struct {
	database    Database
	storagepath string
}

func OpenBackend(DSN, StoragePath string) (backend Backend, err error) {
	if backend.database, err = OpenDatabase(DSN); err != nil {
		return
	}
	if err = backend.database.Ping(); err != nil {
		return
	}
	backend.storagepath = StoragePath
	return
}

func (b Backend) Close() {
	b.database.Close()
}

func (b Backend) GetCountStats() (count, views, tags uint, err error) {
	return b.database.GetCountStats()
}

func (b Backend) GetPost(pid uint, includeTags bool) (BooruPost, error) {
	post, err := b.database.GetPost(pid)
	if err == nil && includeTags {
		post.Tags, err = b.database.GetPostTags(pid)
	}
	return post, err
}

func (b Backend) SearchPosts(term string, offset, count uint, sess Session) ([]SearchPost, uint, bool, error) {
	return b.database.SearchPosts(term, offset, count, sess)
}

func (b Backend) SetImage(pid uint, imgData []byte) error {
	// delete old image and thumbnail
	post, err := b.GetPost(pid, false)
	if err != nil {
		return err
	}
	if err = os.RemoveAll(b.GetThumbPath(pid, post.ThumbMime, false)); err != nil {
		return err
	}
	if err = os.RemoveAll(b.GetImagePath(pid, post.ImageMime, false)); err != nil {
		return err
	}
	// get mime type of new image
	mimeType, mimeSubType, err := mediainfo.GetMime(imgData)
	if err != nil {
		return err
	}
	// save image mime in post
	post.ImageMime = mimeType + "/" + mimeSubType
	// save image
	imgPath := b.GetImagePath(pid, post.ImageMime, false)
	imgFile, err := os.Create(imgPath)
	if err != nil {
		return err
	}
	_, err = imgFile.Write(imgData)
	imgFile.Close()
	if err != nil {
		return err
	}
	// get media info
	info, err := mediainfo.GetInfo(imgData, imgPath, mimeType, mimeSubType)
	if err != nil {
		return err
	}
	// store some media info in post
	post.Width, post.Height = uint(info.Width), uint(info.Height)
	post.Hash, post.PHash = info.Hash, info.PHash
	// save thumbnail (single thumb as jpg, multiple thumbs as gif)
	// if no thumbnail was generated, check for flash mime and use flash thumb
	if len(info.Thumbnails) > 1 {
		post.ThumbMime = mediainfo.MimeGif
		thumbPath := b.GetThumbPath(pid, post.ThumbMime, false)
		err = imaging.SaveAnimGIF(info.Thumbnails, 50, thumbPath)
	} else if len(info.Thumbnails) == 1 {
		post.ThumbMime = mediainfo.MimeJpeg
		thumbPath := b.GetThumbPath(pid, post.ThumbMime, false)
		err = imaging.SaveJPG(info.Thumbnails[0], thumbPath)
	// } else if post.ImageMime == mediainfo.MimeFlash {
		// post.ThumbMime = mediainfo.MimeJpeg
		// thumbPath := b.GetThumbPath(pid, post.ThumbMime, false)
		//TODO copy flash thumbnail to thumbPath
	}
	if err != nil {
		return err
	}
	// store image meta in database
	return b.database.UpdateImageMeta(post)
}

func (b Backend) CreatePost(uid uuid.UUID) (uint, error) {
	return b.database.InsertPost(uid)
}

func (b Backend) UpdatePostMeta(bp BooruPost) error {
	return b.database.UpdatePostMeta(bp)
}

func (b Backend) DeletePost(bp BooruPost) error {
	if err := b.database.DeletePostTags(bp.PID); err != nil {
		return err
	}
	if err := b.database.DeletePost(bp.PID); err != nil {
		return err
	}
	if err := os.RemoveAll(b.GetThumbPath(bp.PID, bp.ThumbMime, false)); err != nil {
		return err
	}
	if err := os.RemoveAll(b.GetImagePath(bp.PID, bp.ImageMime, false)); err != nil {
		return err
	}
	return nil
}

func (b Backend) ResolveTag(tag string, resolveAlias bool, createTag bool) (BooruTag, bool, error) {
	if resolveAlias {
		tid, ok, err := b.database.ResolveAlias(tag)
		if err != nil {
			return BooruTag{}, false, err
		}
		if ok {
			btag, err := b.database.GetTag(tid)
			return btag, true, err
		}
	}
	btag, ok, err := b.database.ResolveTag(tag)
	if ok || err != nil {
		return btag, ok, err
	}
	if !createTag {
		return btag, false, nil
	}
	btag, err = b.database.InsertTag(tag)
	return btag, err == nil, err
}

func (b Backend) SetPostTags(pid uint, tags []string) error {
	if err := b.database.DeletePostTags(pid); err != nil {
		return err
	}
	tids := make([]uint, len(tags))
	for i, tag := range tags {
		btag, _, err := b.ResolveTag(tag, true, true)
		if err != nil {
			return err
		}
		tids[i] = btag.TID
	}
	return b.database.InsertPostTags(pid, tids)
}

func (b Backend) GetTags() ([]BooruTag, error) {
	return b.database.GetTags()
}

func (b Backend) GetAliases() ([]BooruAlias, error) {
	return b.database.GetAliases()
}
