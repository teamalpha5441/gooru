package backend

import (
	"bitbucket.org/teamalpha5441/Gooru/server/mediainfo"
	"github.com/satori/go.uuid"
	"os"
	"path"
	"strconv"
)

const (
	AVATAR_DIR = "avatars"
	THUMB_DIR  = "thumbs"
	IMAGE_DIR  = "images"

	AVATAR_MIME = mediainfo.MimePng
)

var (
	AVATAR_EXTENSION = mediainfo.FileExtensionFromMime(AVATAR_MIME)
)

func (b Backend) GetAvatarPath(uid uuid.UUID, fallback bool) string {
	fpath := path.Join(b.storagepath, AVATAR_DIR, uid.String()+AVATAR_EXTENSION)
	if _, err := os.Stat(fpath); os.IsNotExist(err) && fallback {
		fpath = path.Join(b.storagepath, AVATAR_DIR, "default"+AVATAR_EXTENSION)
	}
	return fpath
}

func (b Backend) GetThumbPath(pid uint, mime string, fallback bool) string {
	ext := mediainfo.FileExtensionFromMime(mime)
	fpath := path.Join(b.storagepath, THUMB_DIR, strconv.FormatUint(uint64(pid), 10)+ext)
	if _, err := os.Stat(fpath); os.IsNotExist(err) && fallback {
		fpath = path.Join(b.storagepath, THUMB_DIR, "default"+ext)
	}
	return fpath
}

func (b Backend) GetImagePath(pid uint, mime string, fallback bool) string {
	ext := mediainfo.FileExtensionFromMime(mime)
	fpath := path.Join(b.storagepath, IMAGE_DIR, strconv.FormatUint(uint64(pid), 10)+ext)
	if _, err := os.Stat(fpath); os.IsNotExist(err) && fallback {
		fpath = path.Join(b.storagepath, IMAGE_DIR, "default"+ext)
	}
	return fpath
}
