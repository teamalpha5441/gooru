package imaging

import (
	"github.com/disintegration/imaging"
	"image"
	"io"
	"os"
)

func Open(f string) (image.Image, error) {
	return imaging.Open(f)
}

func Read(r io.Reader) (image.Image, error) {
	return imaging.Decode(r)
}

func CreateThumbnail(img image.Image, size int) image.Image {
	return imaging.Fill(img, size, size, imaging.Center, imaging.Lanczos)
}

// see http://www.hackerfactor.com/blog/?/archives/529-Kind-of-Like-That.html
// compare hashes by hamming distance
// MySQL: SELECT id, BIT_COUNT(phash ^ 0x1231231231) AS dist FROM posts
// see http://stackoverflow.com/a/21058895/1502200
// high bit is always set to 0 to circumvent a limitation in MySQL driver
// see https://github.com/golang/go/issues/9373
func CalculatePHash(img image.Image) uint64 {
	// resize grayscale version of img to 9x8
	i := imaging.Resize(imaging.Grayscale(img), 9, 8, imaging.Lanczos)
	h := uint64(0)
	for y := 0; y < 8; y++ {
		for x := 0; x < 8; x++ {
			// extract brightness (taken from green channel) and alpha
			_, g1, _, a1 := i.At(x, y).RGBA()
			_, g2, _, a2 := i.At(x+1, y).RGBA()
			// alpha blend transparency with booru background gray
			g1 += (0xffff - a1) * 0x2c
			g2 += (0xffff - a2) * 0x2c
			// compare and maybe set bit in hash
			if g1 < g2 {
				h |= 1 << uint8(63-y*8-x)
			}
		}
	}
	// clear high bit and return
	return h &^ (1 << 63);
}

func SaveJPG(img image.Image, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	return imaging.Encode(file, img, imaging.JPEG)
}
