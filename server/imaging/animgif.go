package imaging

import (
	"errors"
	"image"
	"image/gif"
	"os"
)

var (
	ErrImageNotPaletted = errors.New("imaging: image not paletted")
)

func SaveAnimGIF(img []image.Image, delay_ms int, path string) error {
	g := &gif.GIF{}
	for _, i := range img {
		ip, ok := i.(*image.Paletted)
		if !ok {
			return ErrImageNotPaletted
		}
		g.Image = append(g.Image, ip)
		g.Delay = append(g.Delay, delay_ms)
	}

	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()

	return gif.EncodeAll(f, g)
}
