package main

import (
	"bitbucket.org/teamalpha5441/Gooru/server/backend"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func parseUintFromGetSearchHandler(w http.ResponseWriter, r *http.Request, name string, ptr *uint) bool {
	if len(r.FormValue(name)) > 0 {
		tmp, err := strconv.ParseUint(r.FormValue(name), 10, 32)
		if err != nil {
			errmsg := "couldn't parse offset"
			handlerError(w, http.StatusBadRequest, errmsg, errmsg)
			return false
		}
		*ptr = uint(tmp)
	}
	return true
}

func handlerGetSearch(w http.ResponseWriter, r *http.Request) {
	var query string
	if tmp, ok := mux.Vars(r)["query"]; ok {
		query = tmp
	}
	offset, count := uint(0), uint(0)
	if ok := parseUintFromGetSearchHandler(w, r, "offset", &offset); !ok {
		return
	}
	if ok := parseUintFromGetSearchHandler(w, r, "count", &count); !ok {
		return
	}
	sess, ok := handlerGetSession(w, r, false)
	if !ok {
		return
	}

	posts, total, is_srv_error, err := Backend.SearchPosts(query, offset, count, sess)
	if err != nil {
		if is_srv_error {
			handlerError(w, http.StatusInternalServerError, err.Error(), "search query execution failed")
		} else {
			handlerError(w, http.StatusBadRequest, err.Error(), err.Error())
		}
		return
	}
	data := struct {
		Count int
		Total uint
		Posts []backend.SearchPost
	}{
		len(posts),
		total,
		posts,
	}
	handlerReturnJson(w, data)
}
