package main

import (
	"bitbucket.org/teamalpha5441/Gooru/server/backend"
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strings"
)

const (
	ERROR_ID_LETTERS = "abcdefghklmnpqrstuvwxyz23456789"
)

var (
	ERRMSG_TOKEN_EMPTY  = "session token empty"
	ERRMSG_TOKEN_FORMAT = "session token badly formatted"
	ERRMSG_UNAUTHORIZED = "not authenticated, but authentication needed"
)

func generateRandomErrorIdentifier() string {
	b := make([]byte, 8)
	for i := range b {
		r := rand.Intn(len(ERROR_ID_LETTERS))
		b[i] = ERROR_ID_LETTERS[r]
	}
	return string(b)
}

func handlerError(w http.ResponseWriter, http_status int, verbose_msg, user_msg string) {
	error_id := generateRandomErrorIdentifier()
	log.Println("ErrorID:", error_id, "-", verbose_msg)
	http.Error(w, "ErrorID: "+error_id+" - "+user_msg, http_status)
}

func handlerGetSession(w http.ResponseWriter, r *http.Request, needed bool) (backend.Session, bool) {
	var authString string
	if authHeaders, ok := r.Header["Authorization"]; ok && len(authHeaders) > 0 {
		if !strings.HasPrefix(authHeaders[0], "Booru ") {
			handlerError(w, http.StatusBadRequest, ERRMSG_TOKEN_FORMAT, ERRMSG_TOKEN_FORMAT)
			return backend.GUEST_SESSION, false
		}
		if len(authHeaders[0]) < 7 {
			handlerError(w, http.StatusBadRequest, ERRMSG_TOKEN_EMPTY, ERRMSG_TOKEN_EMPTY)
			return backend.GUEST_SESSION, false
		}
		authString = authHeaders[0][6:]
	} else if authCookie, err := r.Cookie("Authorization"); err == nil {
		if len(authCookie.Value) < 1 {
			handlerError(w, http.StatusBadRequest, ERRMSG_TOKEN_EMPTY, ERRMSG_TOKEN_EMPTY)
			return backend.GUEST_SESSION, false
		}
		authString = authCookie.Value
	}
	if len(authString) > 0 {
		sess, err := Backend.GetSession(authString)
		if err != nil {
			handlerError(w, http.StatusUnauthorized, err.Error(), "authentication failed")
			return backend.GUEST_SESSION, false
		}
		return sess, true
	}
	if !needed {
		return backend.GUEST_SESSION, true
	}
	handlerError(w, http.StatusUnauthorized, ERRMSG_UNAUTHORIZED, ERRMSG_UNAUTHORIZED)
	return backend.GUEST_SESSION, false
}

func handlerReturnJson(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	if json, err := json.Marshal(data); err == nil {
		w.Write(json)
	} else {
		handlerError(w, http.StatusInternalServerError, err.Error(), "json marshalling error")
	}
}
