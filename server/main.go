package main

import (
	"bitbucket.org/teamalpha5441/Gooru/server/backend"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

type FileConfig struct {
	ListenAddress string
	StoragePath   string
	DatabaseDSN   string
	URLPrefix     string
}

const (
	API_VERSION_MAJOR = 5
	API_VERSION_MINOR = 1
	REGEX_UUID  = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
)

var (
	Config  FileConfig
	Backend backend.Backend
)

func init() {
	// disable log prefix
	log.SetPrefix("")
	log.SetFlags(0)

	// read config file
	if len(os.Args) < 2 {
		panic("no config path provided as argument")
	}
	file, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer file.Close()
	if err = json.NewDecoder(file).Decode(&Config); err != nil {
		panic(err)
	}
}

func main() {
	var err error
	Backend, err = backend.OpenBackend(Config.DatabaseDSN, Config.StoragePath)
	if err != nil {
		panic(err)
	}
	defer Backend.Close()
	log.Println("Backend opened successfull")

	router := mux.NewRouter()
	router.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		errmsg := "unknown api call"
		handlerError(w, http.StatusBadRequest, errmsg, errmsg)
	})

	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/info").HandlerFunc(handlerGetInfo)

	router.Methods(http.MethodPost).Path(Config.URLPrefix + "/session/login").HandlerFunc(handlerPostSessionLogin)
	router.Methods(http.MethodPost).Path(Config.URLPrefix + "/session/logout").HandlerFunc(handlerPostSessionLogout)

	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/users/{uid:" + REGEX_UUID + "}/avatar").HandlerFunc(handlerGetUsersAvatar)
	router.Methods(http.MethodPut).Path(Config.URLPrefix + "/users/{uid:" + REGEX_UUID + "}/avatar").HandlerFunc(handlerPutUsersAvatar)
	router.Methods(http.MethodDelete).Path(Config.URLPrefix + "/users/{uid:" + REGEX_UUID + "}/avatar").HandlerFunc(handlerDeleteUsersAvatar)

	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/posts").HandlerFunc(handlerGetSearch)
	router.Methods(http.MethodPost).Path(Config.URLPrefix + "/posts").HandlerFunc(handlerPostPosts)
	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/search/{query}").HandlerFunc(handlerGetSearch)

	router.Methods(http.MethodDelete).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}").HandlerFunc(handlerDeletePosts)
	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}/meta").HandlerFunc(handlerGetPostsMeta)
	router.Methods(http.MethodPut).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}/meta").HandlerFunc(handlerPutPostsMeta)
	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}/thumb").HandlerFunc(handlerGetPostsThumb)
	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}/image").HandlerFunc(handlerGetPostsImage)
	router.Methods(http.MethodPut).Path(Config.URLPrefix + "/posts/{pid:[0-9]+}/image").HandlerFunc(handlerPutPostsImage)

	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/extra/tags").HandlerFunc(handlerGetExtraTags)
	router.Methods(http.MethodGet).Path(Config.URLPrefix + "/extra/aliases").HandlerFunc(handlerGetExtraAliases)

	// ch := make(chan os.Signal)
	// signal.Notify(ch, syscall.SIGTERM)
	log.Println("Listening now ...")
	panic(http.ListenAndServe(Config.ListenAddress, router))
}
