package mediainfo

import (
	"fmt"
	"image"
	"image/gif"
	"os"
	"path"
	"strconv"
)

const (
	codecTypeVideo = "video"
)

func getVideoInfo(file string) (int, int, float64, error) {
	result, err := ffprobeGetInfo(file)
	if err != nil {
		return 0, 0, 0, err
	}
	w, h, video_found := 0, 0, false
	for _, s := range result.Streams {
		if s.CodecType == codecTypeVideo {
			if video_found {
				return 0, 0, 0, ErrMultipleVideoStreams
			}
			w, h, video_found = s.Width, s.Height, true
		}
	}
	if !video_found {
		return 0, 0, 0, ErrNoVideoStreams
	}
	duration, err := strconv.ParseFloat(result.Format.Duration, 64)
	if err != nil {
		return 0, 0, duration, err
	}
	return w, h, duration, nil
}

func calcThumbDelayAndCount(duration float32) (delay int, count int) {
	if duration < 5 {
		return 0, 1
	}
	count = int(duration/6 + 0.5)
	delay = int(duration/float32(count+1) + 0.5)
	return
}

func createVideoThumbnails(file string, video_duration float32, video_size_min, thumb_size int) (thumbs []image.Image, err error) {
	frame_delay_s, count := calcThumbDelayAndCount(video_duration)
	var tmpdir, filefmt string
	tmpdir, filefmt, err = ffmpegCreateThumbnails(file, video_size_min, thumb_size, frame_delay_s)
	defer func() {
		if err == nil {
			err = os.RemoveAll(tmpdir)
		} else {
			os.RemoveAll(tmpdir)
		}
	}()
	if err != nil {
		return nil, err
	}
	thumbs = make([]image.Image, count)
	var imgfile *os.File
	for i := 0; i < count; i++ {
		pngpath := fmt.Sprintf(path.Join(tmpdir, filefmt), i+1)
		gifpath := pngpath + ".gif"
		// go doesn't support png -> gif conversion, use imagemagick
		if err = convertPngToGif(pngpath, gifpath); err != nil {
			return thumbs, err
		}
		imgfile, err = os.Open(gifpath)
		if err != nil {
			return thumbs, err
		}
		thumbs[i], err = gif.Decode(imgfile)
		if err != nil {
			return thumbs, err
		}
		imgfile.Close()
	}
	return thumbs, nil
}
