package mediainfo

import (
	"errors"
)

var (
	ErrUnknownMimeType      = errors.New("mediainfo: unknown mime type")
	ErrNoMimeHandler        = errors.New("mediainfo: no handler for mime type") // should never happen
	ErrNoVideoStreams       = errors.New("mediainfo: no video streams in video")
	ErrMultipleVideoStreams = errors.New("mediainfo: multiple video streams in video")
	ErrUnknownFlashVersion  = errors.New("mediainfo: unknown swf version")
	ErrLengthsMismatching   = errors.New("mediainfo: header and file lengths mismatching")
	ErrNotEnoughData        = errors.New("mediainfo: not enough data or array too short")
)
