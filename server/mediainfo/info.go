package mediainfo

import (
	"bitbucket.org/teamalpha5441/Gooru/server/imaging"
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"image"
	"io"
)

const (
	thumbSize = 128
)

type MediaInfo struct {
	// set for everything
	Hash          string
	Width, Height int
	// set for images (one thumbnail) and videos (multiple thumbnails)
	Thumbnails []image.Image
	// set for images only
	PHash uint64
}

func GetMime(data []byte) (mimeType, mimeSubType string, err error) {
	if len(data) < 10 {
		return "", "", ErrNotEnoughData
	}
	var ok bool
	mimeType, mimeSubType, ok = detectMimeType(data)
	if !ok {
		err = ErrUnknownMimeType
	}
	return
}

func GetInfo(data []byte, file, mimeType, mimeSubType string) (MediaInfo, error) {
	var info MediaInfo

	// calculate sha256 hash
	hasher := sha256.New()
	if _, err := hasher.Write(data); err != nil {
		return info, err
	}
	hash := hasher.Sum(nil)
	info.Hash = hex.EncodeToString(hash)[:16]

	// handle image
	if mimeType == mimeTypeImage {
		reader := bytes.NewReader(data)
		return handleImage(reader, info)
	}

	// handle video
	if mimeType == mimeTypeVideo {
		return handleVideo(file, info)
	}

	// handle application/x-shockwave-flash
	if mimeType == mimeTypeApplication && mimeSubType == mimeSubTypeShockwaveFlash {
		return handleFlash(data, info)
	}

	// should never happen
	return info, ErrNoMimeHandler
}

func handleImage(reader io.Reader, info MediaInfo) (MediaInfo, error) {
	// load image from reader
	img, err := imaging.Read(reader)
	if err != nil {
		return info, err
	}
	// get image size
	bounds := img.Bounds()
	info.Width = bounds.Dx()
	info.Height = bounds.Dy()
	// create thumbnail
	//TODO create multiple thumbnails for animated gif
	info.Thumbnails = []image.Image{imaging.CreateThumbnail(img, thumbSize)}
	// calculate phash
	info.PHash = imaging.CalculatePHash(img)
	return info, nil
}

func handleVideo(file string, info MediaInfo) (MediaInfo, error) {
	// get size of first video stream
	var duration float64
	var err error
	info.Width, info.Height, duration, err = getVideoInfo(file)
	if err != nil {
		return info, err
	}
	// calculate video_size_min
	video_size_min := info.Width
	if info.Height < info.Width {
		video_size_min = info.Height
	}
	// create thumbnails
	info.Thumbnails, err = createVideoThumbnails(file, float32(duration), video_size_min, thumbSize)
	return info, err
}

func handleFlash(data []byte, info MediaInfo) (MediaInfo, error) {
	var err error
	info.Width, info.Height, err = getFlashSize(data)
	return info, err
}
