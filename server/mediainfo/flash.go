package mediainfo

// see http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/swf/pdf/swf-file-format-spec.pdf
// see http://www-lehre.informatik.uni-osnabrueck.de/~fbstark/diplom/docs/swf/Flash_Uncovered.htm
//     (open twice, redirects to main page if referer is from another site)
// see https://helpx.adobe.com/de/flash-player/kb/exception-thrown-you-decompress-lzma-compressed.html

import (
	"bytes"
	"compress/zlib"
	"github.com/ulikunitz/xz/lzma"
	"io"
)

const (
	typeUncompressedFWS   = 0x46
	typeZlibCompressedCWS = 0x43
	typeLzmaCompressedZWS = 0x5a
)

// read an four byte unsigned int (lsb)
func readUint32(data []byte) uint32 {
	x := uint32(data[0])
	x |= uint32(data[1]) << 8
	x |= uint32(data[2]) << 16
	x |= uint32(data[3]) << 24
	return x
}

// read a single rect value with variable bit length
func readSingleRectValue(data []byte, bitcount, bitoffset int) (x int) {
	for i := 0; i < bitcount; i++ {
		if (data[(i+bitoffset)/8]>>uint(7-(i+bitoffset)%8))&1 > 0 {
			x |= 1 << uint(bitcount-1-i)
		}
	}
	return
}

func readFlashSize(flashSizeBytes []byte) (int, int, error) {
	bitCount := int(flashSizeBytes[0]>>3) & 31
	totalBits := 5 + 4*bitCount
	totalBytes := totalBits / 8
	if totalBits%8 > 0 {
		totalBytes++
	}
	if len(flashSizeBytes) < totalBytes {
		return 0, 0, ErrNotEnoughData
	}
	w := readSingleRectValue(flashSizeBytes, bitCount, 5+bitCount)
	h := readSingleRectValue(flashSizeBytes, bitCount, 5+3*bitCount)
	return w / 20, h / 20, nil
}

func getFlashSizeZlib(data []byte) (int, int, error) {
	// skip eight header bytes and decompress
	reader, err := zlib.NewReader(bytes.NewReader(data[8:]))
	if err != nil {
		return 0, 0, err
	}
	defer reader.Close()
	decompressed := make([]byte, 32)
	if _, err = io.ReadFull(reader, decompressed); err != nil {
		return 0, 0, err
	}
	return readFlashSize(decompressed)
}

// pretty slow, maybe the whole file is decompressed?
func getFlashSizeLzma(data []byte) (int, int, error) {
	// read decompLen from offset four and decrement by eight
	decompLen := readUint32(data[4:8]) - 8
	// move props field from offset twelve to offset four
	data[4], data[5], data[6], data[7], data[8] = data[12], data[13], data[14], data[15], data[16]
	// decompLen is eight bytes long in the correct header
	// save lower four bytes decompLen to offset 13
	data[13] = byte(decompLen & 0xFF)
	data[14] = byte((decompLen >> 8) & 0xFF)
	data[15] = byte((decompLen >> 16) & 0xFF)
	data[16] = byte((decompLen >> 24) & 0xFF)
	// zero four bytes at offset nine
	data[9], data[10], data[11], data[12] = 0, 0, 0, 0
	// skip four header bytes and decompress
	reader, err := lzma.NewReader(bytes.NewReader(data[4:]))
	if err != nil {
		return 0, 0, err
	}
	decompressed := make([]byte, 32)
	if _, err = io.ReadFull(reader, decompressed); err != nil {
		return 0, 0, err
	}
	return readFlashSize(decompressed)
}

func getFlashSize(data []byte) (int, int, error) {
	// absolute bare minimum file length is 10 bytes
	if len(data) < 10 {
		return 0, 0, ErrNotEnoughData
	}
	// second and third byte must equal 'W' and 'S'
	if data[1] != 0x57 || data[2] != 0x53 {
		return 0, 0, ErrUnknownFlashVersion
	}

	switch data[0] {
	case typeUncompressedFWS:
		// read flash size directly
		return readFlashSize(data[8:])
	case typeZlibCompressedCWS:
		// handled in separate method
		return getFlashSizeZlib(data)
	case typeLzmaCompressedZWS:
		// handled in separate method
		return getFlashSizeLzma(data)
	}

	return 0, 0, ErrUnknownFlashVersion
}
