package mediainfo

// needs the following executables: ffprobe, ffmpeg, convert
// normally found in these packages: ffmpeg, imagemagick

// see http://stackoverflow.com/a/8191228/1502200
// see https://trac.ffmpeg.org/wiki/Create%20a%20thumbnail%20image%20every%20X%20seconds%20of%20the%20video

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os/exec"
	"path"
	"strings"
)

const (
	convertPath = "/usr/bin/convert"
	ffprobePath = "/usr/bin/ffprobe"
	ffmpegPath  = "/usr/bin/ffmpeg"
)

type ffprobeResult struct {
	Format struct {
		Duration string `json:duration`
	} `json:format`
	Streams []struct {
		Index     int    `json:index`
		CodecType string `json:"codec_type"`
		Width     int    `json:width`
		Height    int    `json:height`
	} `json:streams`
}

func convertPngToGif(png, gif string) error {
	return exec.Command(convertPath, png, gif).Run()
}

func ffprobeGetInfo(file string) (ffprobeResult, error) {
	var result ffprobeResult
	output, err := exec.Command(ffprobePath, "-v", "quiet", "-print_format", "json", "-show_format", "-show_streams", file).Output()
	if err != nil {
		return result, err
	}
	err = json.Unmarshal(output, &result)
	return result, err
}

func ffmpegCreateThumbnails(file string, video_size_min, thumb_size, frame_delay_s int) (string, string, error) {
	tmpdir, err := ioutil.TempDir("", "gooru_thumbgen_")
	if err != nil {
		return tmpdir, "", err
	}
	// dont output gifs directly, ffmpeg outputs diff frames when using gif as the image format
	// use png and convert to gif later
	filefmt := "frame_%d.png"
	outfmt := path.Join(tmpdir, filefmt)
	vfilters := []string{
		fmt.Sprintf("fps=1/%d", frame_delay_s),
		fmt.Sprintf("crop=%d:%d", video_size_min, video_size_min),
		fmt.Sprintf("scale=%d:%d:flags=lanczos", thumb_size, thumb_size),
	}

	var cmd *exec.Cmd
	if frame_delay_s > 0 {
		cmd = exec.Command(ffmpegPath, "-i", file, "-vf", strings.Join(vfilters, ","), "-c:v", "png", "-f", "image2", outfmt)
	} else {
		cmd = exec.Command(ffmpegPath, "-i", file, "-vframes", "1", outfmt)
	}
	cmd.Stdin, cmd.Stdout, cmd.Stderr = nil, nil, nil
	return tmpdir, filefmt, cmd.Run()
}
