package mediainfo

const (
	mimeTypeImage       = "image"
	mimeTypeVideo       = "video"
	mimeTypeApplication = "application"

	mimeSubTypeJpeg           = "jpeg"
	mimeSubTypePng            = "png"
	mimeSubTypeWebm           = "webm"
	mimeSubTypeGif            = "gif"
	mimeSubTypeShockwaveFlash = "x-shockwave-flash"
	mimeSubTypeOctetStream    = "octet-stream"

	MimeJpeg   = mimeTypeImage + "/" + mimeSubTypeJpeg
	MimePng    = mimeTypeImage + "/" + mimeSubTypePng
	MimeWebm   = mimeTypeVideo + "/" + mimeSubTypeWebm
	MimeGif    = mimeTypeImage + "/" + mimeSubTypeGif
	MimeFlash  = mimeTypeApplication + "/" + mimeSubTypeShockwaveFlash
	MimeBinary = mimeTypeApplication + "/" + mimeSubTypeOctetStream
)

var (
	magicBytes = [][]int16{
		[]int16{0xFF, 0xD8, 0xFF, 0xE0, -1, -1, 0x4A, 0x46, 0x49, 0x46, 0, 1}, // JPEG (JFIF)
		[]int16{0xFF, 0xD8, 0xFF, 0xE1, -1, -1, 0x45, 0x78, 0x69, 0x66, 0, 0}, // JPEG (EXIF)
		[]int16{0xFF, 0xD8, 0xFF, 0xDB},                                       // JPEG (RAW)
		[]int16{0x89, 0x50, 0x4e, 0x47, 0xd, 0xa, 0x1a, 0xa},                  // PNG
		[]int16{0x1A, 0x45, 0xDF, 0xA3},                                       // WEBM
		[]int16{0x47, 0x49, 0x46, 0x38, 0x37, 0x61},                           // GIF 1
		[]int16{0x47, 0x49, 0x46, 0x38, 0x39, 0x61},                           // GIF 2
		[]int16{0x46, 0x57, 0x53},                                             // SWF (FWS)
		[]int16{0x43, 0x57, 0x53},                                             // SWF (CWS)
		[]int16{0x5a, 0x57, 0x53},                                             // SWF (ZWS)
	}
	mimeTypes = []string{
		mimeTypeImage, mimeTypeImage, mimeTypeImage,
		mimeTypeImage,
		mimeTypeVideo,
		mimeTypeImage, mimeTypeImage,
		mimeTypeApplication, mimeTypeApplication, mimeTypeApplication,
	}
	mimeSubTypes = []string{
		mimeSubTypeJpeg, mimeSubTypeJpeg, mimeSubTypeJpeg,
		mimeSubTypePng,
		mimeSubTypeWebm,
		mimeSubTypeGif, mimeSubTypeGif,
		mimeSubTypeShockwaveFlash, mimeSubTypeShockwaveFlash, mimeSubTypeShockwaveFlash,
	}
	mimeExtensions = map[string]string{
		MimeJpeg:  ".jpg",
		MimePng:   ".png",
		MimeWebm:  ".webm",
		MimeGif:   ".gif",
		MimeFlash: ".swf",
	}
)

// only needed by detectMimeType, needs 12 header bytes
func checkMagicBytes(magic []int16, data12b []byte) bool {
	for i := 0; i < len(magic); i++ {
		if magic[i] >= 0 && magic[i] != int16(data12b[i]) {
			return false
		}
	}
	return true
}

// needs 12 header bytes
func detectMimeType(data12b []byte) (string, string, bool) {
	for i, m := range magicBytes {
		if checkMagicBytes(m, data12b) {
			return mimeTypes[i], mimeSubTypes[i], true
		}
	}
	return mimeTypeApplication, mimeSubTypeOctetStream, false
}

func FileExtensionFromMime(mime string) string {
	if suffix, ok := mimeExtensions[mime]; ok {
		return suffix
	}
	return ".bin"
}
